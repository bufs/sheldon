package com.poochie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sheldon {

  public static void main(String[] args) {
    SpringApplication.run(Sheldon.class, args);
  }
}
