package com.poochie.component.district;

import com.poochie.domain.districts.City;
import com.poochie.domain.districts.Country;
import com.poochie.domain.districts.Province;
import java.util.List;

public interface DistrictComponent {

  List<City> getAllCitiesByNameAndProvince(final String name, final long provinceId);

  List<Province> getAllProvincesByName(final String name);

  List<Country> getAllCountries();
}
