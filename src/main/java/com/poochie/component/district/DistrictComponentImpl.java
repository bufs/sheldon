package com.poochie.component.district;

import com.poochie.domain.districts.City;
import com.poochie.domain.districts.Country;
import com.poochie.domain.districts.Province;
import com.poochie.domain.repositories.CityRepository;
import com.poochie.domain.repositories.CountryRepository;
import com.poochie.domain.repositories.ProvinceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DistrictComponentImpl implements DistrictComponent {

  private final CityRepository cityRepository;
  private final CountryRepository countryRepository;
  private final ProvinceRepository provinceRepository;

  @Autowired
  public DistrictComponentImpl(final CityRepository cityRepository,
                               final CountryRepository countryRepository,
                               final ProvinceRepository provinceRepository) {
    this.cityRepository = cityRepository;
    this.countryRepository = countryRepository;
    this.provinceRepository = provinceRepository;
  }

  public List<City> getAllCitiesByNameAndProvince(final String name, final long provinceId) {
    return cityRepository.findCityByNameLikeAndProvinceId(name, provinceId);
  }

  public List<Province> getAllProvincesByName(final String name) {
    return provinceRepository.findAllByNameLike(name);
  }

  public List<Country> getAllCountries() {
    return countryRepository.findAll();
  }
}
