package com.poochie.component.filter;

import com.poochie.domain.districts.Ubiety;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DynamicFilter {

  private Ubiety ubiety;

  private boolean illuminated;

  private String address;

  public DynamicFilter(final Ubiety ubiety, final boolean illuminated) {
    this.ubiety = ubiety;
    this.illuminated = illuminated;
  }
}
