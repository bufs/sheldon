package com.poochie.component.filter;

import com.poochie.domain.supporter.Supporter;
import java.util.List;

public interface FilterComponent {

  List<Supporter> findSupportersByCriteria(final long userId, final DynamicFilter dynamicFilter);
}
