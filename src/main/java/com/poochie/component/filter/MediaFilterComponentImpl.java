package com.poochie.component.filter;

import static java.util.stream.Collectors.toList;

import com.poochie.component.outdoor.MediaComponent;
import com.poochie.domain.districts.Ubiety;
import com.poochie.domain.outdoor.OutdoorMedia;
import com.poochie.domain.supporter.Supporter;
import java.util.Comparator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MediaFilterComponentImpl implements FilterComponent {

  private final MediaComponent mediaComponent;

  @Autowired
  private MediaFilterComponentImpl(final MediaComponent mediaComponent) {
    this.mediaComponent = mediaComponent;
  }

  public List<Supporter> findSupportersByCriteria(final long userId, final DynamicFilter dynamicFilter) {
    List<OutdoorMedia> mediaComponents = getMediaComponents(userId);

    List<Supporter> supporters = mediaComponents.stream()
        .map(OutdoorMedia::getSupporter)
        .sorted(Comparator.comparing(Supporter::getFinalCost)
            .thenComparing(supporter -> supporter.getCoverage().getCoverageNumber()))
        .collect(toList());

    List<Supporter> byUbiety = supportersByUbiety(supporters, dynamicFilter.getUbiety());
    return supportersByIlluminated(byUbiety, dynamicFilter.isIlluminated());
  }

  private List<Supporter> supportersByUbiety(final List<Supporter> supporters, final Ubiety ubiety) {
    return supporters
        .stream()
        .filter(supporter -> ubiety.equals(supporter.getPlace().getUbiety()))
        .collect(toList());
  }

  private List<Supporter> supportersByIlluminated(final List<Supporter> supporters, final boolean illuminated) {
    return supporters
        .stream()
        .filter(supporter -> supporter.getIlluminated().equals(illuminated))
        .collect(toList());
  }

  private List<OutdoorMedia> getMediaComponents(final long userId) {
    return mediaComponent.findAllByUserIdInCache(userId);
  }
}
