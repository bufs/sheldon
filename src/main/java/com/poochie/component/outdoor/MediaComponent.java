package com.poochie.component.outdoor;

import com.poochie.domain.excel.OutdoorSheet;
import com.poochie.domain.outdoor.OutdoorMedia;
import java.util.List;

public interface MediaComponent {

  List<OutdoorMedia> persistMedia(List<OutdoorSheet> outdoorSheets);

  List<OutdoorMedia> findAllByUserIdInCache(final long userId);

  List<OutdoorMedia> findAllByUserId(final long userId);
}
