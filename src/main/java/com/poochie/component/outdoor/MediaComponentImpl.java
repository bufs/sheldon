package com.poochie.component.outdoor;

import static java.util.stream.Collectors.toList;

import com.poochie.domain.excel.OutdoorSheet;
import com.poochie.domain.outdoor.OutdoorMedia;
import com.poochie.domain.outdoor.OutdoorMediaMapper;
import com.poochie.domain.repositories.OutdoorMediaRepository;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class MediaComponentImpl implements MediaComponent {

  private static final Logger log = LoggerFactory.getLogger(MediaComponentImpl.class);

  private final OutdoorMediaRepository mediaRepository;
  private final OutdoorMediaMapper mediaMapper;

  @Autowired
  public MediaComponentImpl(final OutdoorMediaRepository mediaRepository,
                            final OutdoorMediaMapper outdoorMediaMapper) {
    this.mediaRepository = mediaRepository;
    this.mediaMapper = outdoorMediaMapper;
  }

  @Override
  public List<OutdoorMedia> persistMedia(List<OutdoorSheet> outdoorSheets) {
    log.info("Processing {} sheets.", outdoorSheets.size());

    List<OutdoorMedia> outdoorMediaList =
        outdoorSheets
            .stream()
            .map(mediaMapper::transformToDomainObject)
            .collect(toList());

    return mediaRepository.save(outdoorMediaList);
  }

  @Override
  @Cacheable(value = "media")
  public List<OutdoorMedia> findAllByUserIdInCache(final long userId) {
    return mediaRepository.findAllByUserId(userId);
  }

  @Override
  public List<OutdoorMedia> findAllByUserId(final long userId) {
    return mediaRepository.findAllByUserId(userId);
  }
}
