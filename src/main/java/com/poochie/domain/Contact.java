package com.poochie.domain;

import com.poochie.domain.districts.CountryCode;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Email;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
public class Contact {

  @Id
  @GeneratedValue
  @ApiModelProperty(hidden = true)
  private Long id;

  private String contact;

  @NotNull
  @Email
  private String email;

  @Enumerated(EnumType.STRING)
  private CountryCode countryCode;

  private String phone;
}
