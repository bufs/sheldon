package com.poochie.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Job {

  @Id
  @GeneratedValue
  private Long id;
  private String job;
  private String hierarchy;

  @OneToMany(mappedBy = "job")
  @JsonIgnore
  private Set<User> users;
}
