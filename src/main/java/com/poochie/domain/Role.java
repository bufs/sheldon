package com.poochie.domain;

public enum Role {
  AUDITOR, GESTOR, MEDIO, ANUNCIANTE, ADMIN
}
