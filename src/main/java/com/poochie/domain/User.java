package com.poochie.domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@EqualsAndHashCode
public class User {

  @Id
  @GeneratedValue
  private Long id;

  private String name;

  private String lastName;

  private String email;

  private String password;

  @Enumerated(EnumType.STRING)
  private Role role;

  @ManyToOne(cascade = CascadeType.ALL)
  private Job job;

  private User() {
    //required for JPA
  }

  public User(final String name, final String lastName, final String email,
              final String password, final Role role, final Job job) {
    this.name = name;
    this.lastName = lastName;
    this.email = email;
    this.password = password;
    this.role = role;
    this.job = job;
  }
}


