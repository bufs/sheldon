package com.poochie.domain.company;

import com.poochie.domain.Contact;
import com.poochie.domain.supporter.Supporter;
import io.swagger.annotations.ApiModelProperty;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.URL;

@Entity
@Getter
@Setter
public class Company {

  @Id
  @GeneratedValue
  @ApiModelProperty(hidden = true)
  private Long id;

  @NotNull
  private String name;

  private String webGroup;

  @NotNull
  @URL
  private String url;

  @Valid
  @OneToOne(cascade = CascadeType.ALL)
  private Contact contact;

  @Valid
  @OneToOne(cascade = CascadeType.ALL)
  private Contact secondContact;

  @Valid
  @NotNull
  @OneToMany(cascade = CascadeType.ALL)
  private Set<Supporter> supporter;
}
