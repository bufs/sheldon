package com.poochie.domain.company;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 * Created by tomas.lingotti on 29/07/17.
 */
@Entity
@Immutable
@Getter
@Setter
public class CompanyGroup {

  @Id
  @GeneratedValue
  private Long id;
  @OneToOne(cascade = CascadeType.ALL)
  private CompanySection companySection;
  private String name;
}
