package com.poochie.domain.company;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by tomas.lingotti on 29/07/17.
 */
@Entity
@Immutable
@Getter
@Setter
public class CompanySection {

  @Id
  @GeneratedValue
  private Long id;
  private String name;
}
