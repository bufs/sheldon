package com.poochie.domain.coverage;

import io.swagger.annotations.ApiModelProperty;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Arrange {

  @Id
  @GeneratedValue
  @ApiModelProperty(hidden = true)
  private Long id;

  @Enumerated(EnumType.STRING)
  private Days day;

  private String start;

  private String end;
}