package com.poochie.domain.coverage;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
@Getter
@Setter
public class Coverage {

  @Id
  @GeneratedValue
  private Long id;

  private Double coverageNumber;

  private String coverageUrl;

  @OneToMany(cascade = CascadeType.ALL)
  private List<Arrange> arranges; //days and hours

  @Enumerated(EnumType.STRING)
  private CoverageType coverageType;
}
