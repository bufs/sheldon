package com.poochie.domain.coverage;

/**
 * Created by tomas.lingotti on 25/07/17.
 */
public enum CoverageType {
  BY_SECOND, BY_MINUTE, BY_HOUR, BY_DAY, BY_WEEK, BY_MONTH
}
