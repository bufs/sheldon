package com.poochie.domain.coverage;

import com.poochie.domain.districts.City;
import com.poochie.domain.districts.Country;
import com.poochie.domain.districts.Province;
import com.poochie.domain.districts.Ubiety;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Getter
@Setter
public class Place {

  @Id
  @GeneratedValue
  private Long id;

  @Fetch(FetchMode.JOIN)
  @OneToOne
  private Country country;

  @Fetch(FetchMode.JOIN)
  @OneToOne
  private Province province;

  @Fetch(FetchMode.JOIN)
  @OneToOne
  private City city;

  private String name;

  private Double latitude;

  private Double longitude;

  @Enumerated(EnumType.STRING)
  private Ubiety ubiety;

}
