package com.poochie.domain.districts;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "cities")
@Entity
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
public class City extends District implements Serializable {

  private static final long serialVersionUID = 1234321L;

  @Id
  private Long id;

  private String name;

  @Column(name = "postal_code")
  private String postalCode;

  @OneToOne
  private Province province;

  @Override
  protected String districtName() {
    return super.getName();
  }

  @Override
  protected String districtType() {
    return super.getType();
  }
}
