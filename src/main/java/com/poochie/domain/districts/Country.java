package com.poochie.domain.districts;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "countries")
@Entity
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
public class Country extends District implements Serializable {

  private static final long serialVersionUID = -324566789895L;

  @Id
  private Long id;
  private String name;
  private String code;

  @Override
  protected String districtName() {
    return super.getName();
  }

  @Override
  protected String districtType() {
    return super.getType();
  }
}
