package com.poochie.domain.districts;

/**
 * Created by tomaslingotti on 7/21/17.
 */
public enum CountryCode {
  AR, US, CO, UR, BR, CH
}
