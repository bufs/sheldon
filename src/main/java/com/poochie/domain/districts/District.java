package com.poochie.domain.districts;

import java.io.Serializable;


public abstract class District implements Serializable {

  private static final long serialVersionUID = 19191231231L;

  private String name;
  private String type;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  protected abstract String districtName();

  protected abstract String districtType();
}
