package com.poochie.domain.districts;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "provinces")
@Entity
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
public class Province extends District implements Serializable {

  private static final long serialVersionUID = 3447454568725L;

  @Id
  private Long id;
  private String name;
  @OneToOne
  private Country country;

  @Override
  protected String districtName() {
    return super.getName();
  }

  @Override
  protected String districtType() {
    return super.getType();
  }
}
