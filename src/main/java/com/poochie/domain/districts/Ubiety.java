package com.poochie.domain.districts;

/**
 * Created by tomi on 31/8/16.
 */
public enum Ubiety {

  AVENUE("Avenida"), STREET("Calle"), PED_ZONE("Peatonal"), COAST("Costanera");

  private final String name;

  Ubiety(final String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
