package com.poochie.domain.excel;

import com.poochie.domain.User;
import com.poochie.domain.media.Interest;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class OutdoorSheet {

  private String mediaId;

  private String urlToPublish; 

  private String supporter; 

  private String size; 

  private String imageUri; 

  private String videoUri; 

  private boolean illuminated; 

  private String freq; 

  private double coverage; 

  private String coverageUrl; 

  private String sexSegmentation;

  private String ageSegmentation;

  private String nseSegmentation;

  private String sector; 

  private String area; 

  private String position;  // professionSegmentation segmentation

  private List<Interest> interests; 

  private double finalCost;

  private User user;
}
