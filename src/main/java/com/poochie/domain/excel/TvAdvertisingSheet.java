package com.poochie.domain.excel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TvAdvertisingSheet {

  private String mediaId;
  private String advertisingSpace;
  private String sizeInPx;
  private String imageUri;
  private String videoUri;
  private double finalCostBySecond;
}
