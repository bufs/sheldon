package com.poochie.domain.excel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@ToString
public class TvSheet {

  private String mediaId;
  private String programName;
  private String programType;
  private String imageUri;
  private String videoUri;
  private double startTime;
  private double endTime;
  private List<String> days;
  private String freq;
  private double coverage;
  private String uriCoverageLink;
  private String sexSegmentation;
  private String ageSegmentation;
  private String nseSegmentation;
  private String sector;
  private String area;
  private String position; // professionSegmentation segmentation
  private List<String> interests;
  private Set<TvAdvertisingSheet> tvAdvertisingSheets;
}
