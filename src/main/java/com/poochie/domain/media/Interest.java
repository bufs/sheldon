package com.poochie.domain.media;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Entity
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Interest implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue
  private Long id;
  private String interest;
  @ManyToOne(fetch = FetchType.LAZY)
  private InterestType interestType;

  private Interest() {
    //Required by JPA.
  }

  public Interest (final String interest) {
    this.interest = interest;
  }
}
