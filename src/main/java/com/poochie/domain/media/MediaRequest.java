package com.poochie.domain.media;

import com.poochie.domain.company.Company;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.Valid;

@Entity
@Getter
@Setter
public class MediaRequest {

  @Id
  @GeneratedValue
  @ApiModelProperty(hidden = true)
  private Long id;

  @Valid
  @OneToOne(cascade = CascadeType.ALL)
  private Company company;
}
