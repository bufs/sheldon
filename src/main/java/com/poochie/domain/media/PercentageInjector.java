package com.poochie.domain.media;

import com.poochie.domain.segmentation.Segmentation;
import com.poochie.domain.segmentation.age.AdultPercentage;
import com.poochie.domain.segmentation.age.AgeDescriptor;
import com.poochie.domain.segmentation.age.AgeSegmentation;
import com.poochie.domain.segmentation.age.AncientPercentage;
import com.poochie.domain.segmentation.age.BoyPercentage;
import com.poochie.domain.segmentation.age.PreAdultPercentage;
import com.poochie.domain.segmentation.age.PreTeenPercentage;
import com.poochie.domain.segmentation.age.TeenPercentage;
import com.poochie.domain.segmentation.nse.EmergencyNse;
import com.poochie.domain.segmentation.nse.HighNse;
import com.poochie.domain.segmentation.nse.LowNse;
import com.poochie.domain.segmentation.nse.MediumHighNse;
import com.poochie.domain.segmentation.nse.MediumLowNse;
import com.poochie.domain.segmentation.nse.MediumNse;
import com.poochie.domain.segmentation.nse.NseSegmentationPercentage;
import com.poochie.domain.segmentation.nse.NseSegmentationDescriptor;
import com.poochie.domain.segmentation.sex.FemalePercentage;
import com.poochie.domain.segmentation.sex.MalePercentage;
import com.poochie.domain.segmentation.sex.SexSegmentation;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;

@Component

public class PercentageInjector {

  /**
   * <p>This method should not change any other value except the <b>percentages</b>.</p>
   * <p>Also this method will modify the reference.</p>
   *
   * @param mediaRequest
   */
  /*
  public MediaRequest inject(final MediaRequest mediaRequest) {

    final MediaRequest _mediaRequest = mediaRequest;
    final Segmentation segmentation = _mediaRequest.getCompany().getSupporter().getSegmentation();
    final AgeSegmentation ageSegmentation = segmentation.getAgeSegmentation();
    final SexSegmentation sexSegmentation = segmentation.getSexSegmentation();

    //sex segmentation
    MalePercentage maleQuantity = sexSegmentation.getMalePercentage();
    FemalePercentage femaleQuantity = sexSegmentation.getFemalePercentage();

    //age segmentation
    BoyPercentage boyQuantity = ageSegmentation.getBoyPercentage();
    PreTeenPercentage preTeenQuantity = ageSegmentation.getPreTeenPercentage();
    TeenPercentage teenQuantity = ageSegmentation.getTeenPercentage();
    AdultPercentage adultQuantity = ageSegmentation.getAdultPercentage();
    PreAdultPercentage preAdultQuantity = ageSegmentation.getPreAdultPercentage();
    AncientPercentage ancientQuantity = ageSegmentation.getAncientPercentage();

    //nseSegmentationPercentage segmentation
    final NseSegmentationPercentage nseSegmentationPercentage = _mediaRequest.getCompany().getSupporter().getSegmentation().getNseSegmentationPercentage();

    EmergencyNse emergencyNse = nseSegmentationPercentage.getEmergencyNse();
    LowNse lowNse = nseSegmentationPercentage.getLowNse();
    MediumLowNse mediumLowNse = nseSegmentationPercentage.getMediumLowNse();
    MediumNse mediumNse = nseSegmentationPercentage.getMediumNse();
    MediumHighNse mediumHighNse = nseSegmentationPercentage.getMediumHighNse();
    HighNse highNse = nseSegmentationPercentage.getHighNse();

    Stream<AgeDescriptor> ageStream = Stream.of(boyQuantity, preAdultQuantity, preTeenQuantity, adultQuantity, teenQuantity, ancientQuantity);
    Stream<AgeDescriptor> sexStream = Stream.of(maleQuantity, femaleQuantity);
    Stream<NseSegmentationDescriptor> nseStream = Stream.of(emergencyNse, lowNse, mediumLowNse, mediumNse, mediumHighNse, highNse);

    // verify if sex quantity > age quantity and sex quantity > nseSegmentationPercentage quantity
    verifyQuantities(sexStream, ageStream, nseStream);

    //map the values to the descriptors...
    final Map<String, Integer> sexSegmentationDescriptor = Stream.of(maleQuantity, femaleQuantity)
            .collect(toMap(AgeDescriptor::get, AgeDescriptor::getQuantity));
    final Map<String, Integer> ageSegmentationByDescriptor = Stream.of(boyQuantity, preAdultQuantity, preTeenQuantity, adultQuantity, teenQuantity, ancientQuantity)
            .collect(toMap(AgeDescriptor::get, AgeDescriptor::getQuantity));
    final Map<String, Integer> nseSegmentationDescriptor = Stream.of(emergencyNse, lowNse, mediumLowNse, mediumNse, mediumHighNse, highNse)
            .collect(toMap(NseSegmentationDescriptor::get, NseSegmentationDescriptor::getQuantity));

    //set the final percentages...
    final Map<String, Integer> ageFinalValues = setPercentage(ageSegmentationByDescriptor);
    final Map<String, Integer> nseFinalValues = setPercentage(nseSegmentationDescriptor);
    final Map<String, Integer> sexFinalValues = setPercentage(sexSegmentationDescriptor);

    mapValues(_mediaRequest, sexFinalValues, "sex");
    mapValues(_mediaRequest, ageFinalValues, "age");
    mapValues(mediaRequest, nseFinalValues, "nse");

    return _mediaRequest;
  }

  private void verifyQuantities(Stream<AgeDescriptor> sexStream, Stream<AgeDescriptor> ageStream, Stream<NseSegmentationDescriptor> nseStream) {
    final Integer sexValue = sexStream.map(AgeDescriptor::getQuantity).reduce(0, (l, r) -> l + r);
    final Integer ageValue = ageStream.map(AgeDescriptor::getQuantity).reduce(0, (l, r) -> l + r);
    final Integer nseValue = nseStream.map(NseSegmentationDescriptor::getQuantity).reduce(0, (l, r) -> l + r);

    if (ageValue > sexValue || nseValue > sexValue) {
      throw new RuntimeException("Age values or nse values are greater than sex values... inconsistency founded!");
    }
  }

  private Map<String, Integer> setPercentage(Map<String, Integer> qtysByName) {
    Integer total = qtysByName
            .entrySet()
            .stream()
            .map(Map.Entry::getValue)
            .reduce(0, (l, r) -> l + r);

            */
    /*
    *  total ---- 100%
    *  current ---- X
    *
    *  current * 100 / total = X
    */

    /*
    final Map<String, Integer> finalValues = new HashMap<>();
    qtysByName.forEach((k, v) -> {
      int p = v * 100 / total;
      finalValues.put(k, p);
    });

    return finalValues;
  }

  private void mapValues(MediaRequest mediaRequest, Map<String, Integer> finalValues, final String descriptor) {
    if ("sex".equalsIgnoreCase(descriptor)) {
      mediaRequest.getCompany().getSupporter().getSegmentation().getSexSegmentation().getMalePercentage().setPercentage(finalValues.get(MalePercentage.NAME));
      mediaRequest.getCompany().getSupporter().getSegmentation().getSexSegmentation().getFemalePercentage().setPercentage(finalValues.get(FemalePercentage.NAME));
    }

    if ("nse".equalsIgnoreCase(descriptor)) {
      mediaRequest.getCompany().getSupporter().getSegmentation().getNseSegmentationPercentage().getEmergencyNse().setPercentage(finalValues.get(EmergencyNse.NAME));
      mediaRequest.getCompany().getSupporter().getSegmentation().getNseSegmentationPercentage().getLowNse().setPercentage(finalValues.get(LowNse.NAME));
      mediaRequest.getCompany().getSupporter().getSegmentation().getNseSegmentationPercentage().getMediumLowNse().setPercentage(finalValues.get(MediumLowNse.NAME));
      mediaRequest.getCompany().getSupporter().getSegmentation().getNseSegmentationPercentage().getMediumNse().setPercentage(finalValues.get(MediumNse.NAME));
      mediaRequest.getCompany().getSupporter().getSegmentation().getNseSegmentationPercentage().getMediumHighNse().setPercentage(finalValues.get(MediumHighNse.NAME));
      mediaRequest.getCompany().getSupporter().getSegmentation().getNseSegmentationPercentage().getHighNse().setPercentage(finalValues.get(HighNse.NAME));
    }

    if ("age".equalsIgnoreCase(descriptor)) {
      mediaRequest.getCompany().getSupporter().getSegmentation().getAgeSegmentation().getBoyPercentage().setPercentage(finalValues.get(BoyPercentage.NAME));
      mediaRequest.getCompany().getSupporter().getSegmentation().getAgeSegmentation().getPreTeenPercentage().setPercentage(finalValues.get(PreTeenPercentage.NAME));
      mediaRequest.getCompany().getSupporter().getSegmentation().getAgeSegmentation().getTeenPercentage().setPercentage(finalValues.get(TeenPercentage.NAME));
      mediaRequest.getCompany().getSupporter().getSegmentation().getAgeSegmentation().getPreAdultPercentage().setPercentage(finalValues.get(PreAdultPercentage.NAME));
      mediaRequest.getCompany().getSupporter().getSegmentation().getAgeSegmentation().getPreAdultPercentage().setPercentage(finalValues.get(AdultPercentage.NAME));
      mediaRequest.getCompany().getSupporter().getSegmentation().getAgeSegmentation().getBoyPercentage().setPercentage(finalValues.get(AncientPercentage.NAME));
    }
  }
*/
}
