package com.poochie.domain.outdoor;

import com.poochie.domain.User;
import com.poochie.domain.company.Company;
import com.poochie.domain.coverage.Place;
import com.poochie.domain.media.Interest;
import com.poochie.domain.supporter.Supporter;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class OutdoorMedia {

  @Id
  @GeneratedValue
  private Long id;

  @OneToOne(cascade = CascadeType.REFRESH)
  private Company company;

  private String url;

  private String imageUri;

  private String videoUri;

  @OneToOne(cascade = CascadeType.ALL)
  private Supporter supporter;

  private String uriCoverageLink;

  @OneToMany
  private Set<Interest> interests;

  private String sector;

  private String area;

  @OneToMany(cascade = CascadeType.ALL)
  private List<Place> nearbyPlaces;

  private Double partialCost;

  @ManyToOne
  @JoinColumn(name = "inserted_by")
  private User user;
}
