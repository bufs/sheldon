package com.poochie.domain.outdoor;

import com.poochie.domain.coverage.Coverage;
import com.poochie.domain.coverage.Place;
import com.poochie.domain.districts.Ubiety;
import com.poochie.domain.excel.OutdoorSheet;
import com.poochie.domain.segmentation.Segmentation;
import com.poochie.domain.segmentation.SegmentationHelper;
import com.poochie.domain.supporter.Supporter;
import org.springframework.stereotype.Service;

@Service
public class OutdoorMediaMapper {

  public OutdoorMedia transformToDomainObject(final OutdoorSheet outdoorSheet) {

    OutdoorMedia media = new OutdoorMedia();
    Supporter supporter = new Supporter();
    Coverage coverage = new Coverage();

    media.setUser(outdoorSheet.getUser());

    Segmentation segmentation = new Segmentation();
    segmentation.setInterests(outdoorSheet.getInterests());
    segmentation.setAgeSegmentation(
        SegmentationHelper.getAgeSegmentation(outdoorSheet.getAgeSegmentation())
    );

    segmentation.setNseSegmentation(
        SegmentationHelper.getNseSegmentation(outdoorSheet.getNseSegmentation())
    );

    segmentation.setSexSegmentation(
        SegmentationHelper.getSexSegmentation(outdoorSheet.getSexSegmentation())
    );

    coverage.setCoverageNumber(outdoorSheet.getCoverage());
    coverage.setCoverageUrl(outdoorSheet.getCoverageUrl());

    supporter.setIlluminated(outdoorSheet.isIlluminated());
    supporter.setFreq(outdoorSheet.getFreq());
    supporter.setCoverage(coverage);
    supporter.setSegmentation(segmentation);
    supporter.setFinalCost(outdoorSheet.getFinalCost());

    Place place = new Place();
    place.setUbiety(Ubiety.AVENUE);
    supporter.setPlace(place);

    media.setUrl(outdoorSheet.getUrlToPublish());
    media.setImageUri(outdoorSheet.getImageUri());
    media.setVideoUri(outdoorSheet.getVideoUri());
    media.setSupporter(supporter);
    media.setSector(outdoorSheet.getSector());
    media.setArea(outdoorSheet.getArea());
    media.setPartialCost(outdoorSheet.getFinalCost());

    return media;
  }
}
