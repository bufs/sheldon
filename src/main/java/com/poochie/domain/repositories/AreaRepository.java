package com.poochie.domain.repositories;

import com.poochie.domain.segmentation.Area;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AreaRepository extends JpaRepository<Area, Long> {
}
