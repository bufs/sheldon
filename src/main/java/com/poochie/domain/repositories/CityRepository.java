package com.poochie.domain.repositories;

import com.poochie.domain.districts.City;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CityRepository extends JpaRepository<City, Long> {

  List<City> findCityByNameLikeAndProvinceId(final String name, final long provinceId);
}
