package com.poochie.domain.repositories;

import com.poochie.domain.company.CompanyActivity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by tomas.lingotti on 30/07/17.
 */
public interface CompanyActivityRepository extends JpaRepository<CompanyActivity, Long> {
}
