package com.poochie.domain.repositories;

import com.poochie.domain.company.Company;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by tomaslingotti on 7/20/17.
 */
public interface CompanyRepository extends JpaRepository<Company, Long> {

}
