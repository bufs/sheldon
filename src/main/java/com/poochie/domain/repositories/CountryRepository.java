package com.poochie.domain.repositories;

import com.poochie.domain.districts.Country;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository extends JpaRepository<Country, Long> {

}
