package com.poochie.domain.repositories;

import com.poochie.domain.media.Interest;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by tomas.lingotti on 26/07/17.
 */
public interface InterestRepository extends JpaRepository<Interest, Long> {
}
