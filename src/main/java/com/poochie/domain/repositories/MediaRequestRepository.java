package com.poochie.domain.repositories;

import com.poochie.domain.media.MediaRequest;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by tomaslingotti on 7/19/17.
 */
public interface MediaRequestRepository extends JpaRepository<MediaRequest, Long> {
}
