package com.poochie.domain.repositories;

import com.poochie.domain.outdoor.OutdoorMedia;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OutdoorMediaRepository extends JpaRepository<OutdoorMedia, Long> {

  List<OutdoorMedia> findAllByUserId(final long userId);
}
