package com.poochie.domain.repositories;

import com.poochie.domain.districts.Province;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProvinceRepository extends JpaRepository<Province, Long> {

  List<Province> findAllByNameLike(final String name);
}
