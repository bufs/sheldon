package com.poochie.domain.repositories;

import com.poochie.domain.SubInterest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubInterestRepository extends JpaRepository<SubInterest, Long> {
}
