package com.poochie.domain.repositories;

import com.poochie.domain.supporter.Supporter;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SupporterRepository extends JpaRepository<Supporter, Long> {

}
