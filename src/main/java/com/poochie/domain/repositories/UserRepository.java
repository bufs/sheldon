package com.poochie.domain.repositories;

import com.poochie.domain.User;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

  Optional<User> findUserByEmailAndPassword(final String email, final String password);
}
