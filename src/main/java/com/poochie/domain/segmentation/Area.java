package com.poochie.domain.segmentation;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Area implements Serializable {

  private static final long serialVersionUID = -143245678L;

  @Id
  private Long id;
  private String area;
  private Integer hierarchy;

}
