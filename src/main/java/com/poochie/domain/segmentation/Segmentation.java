package com.poochie.domain.segmentation;

import com.poochie.domain.media.Interest;
import com.poochie.domain.segmentation.age.AgeSegmentation;
import com.poochie.domain.segmentation.nse.NseSegmentation;
import com.poochie.domain.segmentation.profession.ProfessionSegmentation;
import com.poochie.domain.segmentation.sex.SexSegmentation;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Getter
@Setter
public class Segmentation {

  @Id
  @GeneratedValue
  @ApiModelProperty(hidden = true)
  private Long id;

  @Valid
  @OneToMany(cascade = CascadeType.ALL)
  private List<Interest> interests;

  @Valid
  @NotNull
  @OneToOne(cascade = CascadeType.ALL)
  private AgeSegmentation ageSegmentation;

  @OneToOne(cascade = CascadeType.ALL)
  private ProfessionSegmentation professionSegmentation;

  @OneToOne(cascade = CascadeType.ALL)
  private NseSegmentation nseSegmentation;

  @Valid
  @OneToOne(cascade = CascadeType.ALL)
  private SexSegmentation sexSegmentation;
}
