package com.poochie.domain.segmentation;

import com.poochie.domain.segmentation.age.AgeSegmentation;
import com.poochie.domain.segmentation.nse.NseSegmentation;
import com.poochie.domain.segmentation.sex.SexSegmentation;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class SegmentationHelper {

  public static final String MALE = "hombres";
  public static final String FEMALE = "mujeres";
  public static final String REGEX = ",";

  public static final String EMERGENCY = "emergencia";
  public static final String LOW = "baja";
  public static final String MEDIUM_LOW = "mediabaja";
  public static final String MEDIUM = "media";
  public static final String MEDIUM_HIGH = "mediaalta";
  public static final String HIGH = "alta";

  public static final String BOYS = "chicos";
  public static final String PRE_TEENAGER = "preadolescentes";
  public static final String TEENAGER = "adolescentes";
  public static final String PRE_ADULT = "preadultos";
  public static final String ADULT = "adultos";
  public static final String ANCIENT = "ancianos";

  public static SexSegmentation getSexSegmentation(final String sheetSexSegmentation) {
    String[] sexSegArr = sheetSexSegmentation.split(REGEX);
    List<String> sexSegList = transformToList(sexSegArr);
    SexSegmentation sm = new SexSegmentation();

    if (sexSegList.contains(MALE)) {
      sm.setToM(true);
    }
    if (sexSegList.contains(FEMALE)) {
      sm.setToF(true);
    }

    return sm;
  }

  public static NseSegmentation getNseSegmentation(final String sheetNseSegmentation) {
    String[] nseSegArr = sheetNseSegmentation.trim().split(REGEX);
    List<String> nseSegList = transformToList(nseSegArr);
    NseSegmentation ageSegmentation = new NseSegmentation();

    if (nseSegList.contains(EMERGENCY)) {
      ageSegmentation.setEmergency(true);
    }

    if (nseSegList.contains(LOW)) {
      ageSegmentation.setLow(true);
    }

    if (nseSegList.contains(MEDIUM_LOW)) {
      ageSegmentation.setMediumLow(true);
    }

    if (nseSegList.contains(MEDIUM)) {
      ageSegmentation.setMedium(true);
    }

    if (nseSegList.contains(MEDIUM_HIGH)) {
      ageSegmentation.setMediumHigh(true);
    }

    if (nseSegList.contains(HIGH)) {
      ageSegmentation.setHigh(true);
    }

    return ageSegmentation;
  }

  public static AgeSegmentation getAgeSegmentation(final String sheetAgeSegmentation) {
    String[] ageSegArr = sheetAgeSegmentation.trim().split(REGEX);
    List<String> ageSegList = transformToList(ageSegArr);
    AgeSegmentation ageSegmentation = new AgeSegmentation();

    if (ageSegList.contains(BOYS)) {
      ageSegmentation.setBoy(true);
    }

    if (ageSegList.contains(PRE_TEENAGER)) {
      ageSegmentation.setPreTeenager(true);
    }

    if (ageSegList.contains(TEENAGER)) {
      ageSegmentation.setTeenager(true);
    }

    if (ageSegList.contains(PRE_ADULT)) {
      ageSegmentation.setPreAdult(true);
    }

    if (ageSegList.contains(ADULT)) {
      ageSegmentation.setAdult(true);
    }

    if (ageSegList.contains(ANCIENT)) {
      ageSegmentation.setAncient(true);
    }

    return ageSegmentation;
  }

  private static List<String> transformToList(final String[] arr) {
    return Arrays.stream(arr).map(String::trim).collect(toList());
  }

}
