package com.poochie.domain.segmentation.age;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
@Getter
@Setter
public class AdultPercentage implements AgeDescriptor {

  public static final String NAME = "Adult";

  @Id
  @GeneratedValue
  @ApiModelProperty(hidden = true)
  private Long id;

  private Integer percentage;

  private Integer quantity;

  @Override
  @Transient
  public String get() {
    return NAME;
  }
}
