package com.poochie.domain.segmentation.age;

public interface AgeDescriptor {

  String get();

  Integer getQuantity();
}
