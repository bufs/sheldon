package com.poochie.domain.segmentation.age;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
@Getter
@Setter
public class AgeSegmentation {

  @Id
  @GeneratedValue
  @ApiModelProperty(hidden = true)
  private Long id;

  private Boolean boy;

  private Boolean preTeenager;

  private Boolean teenager;

  private Boolean preAdult;

  private Boolean adult;

  private Boolean ancient;

  @OneToOne(cascade = CascadeType.ALL)
  private AgeSegmentationPercentage percentage;
}
