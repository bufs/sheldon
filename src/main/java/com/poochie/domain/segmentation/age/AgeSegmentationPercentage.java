package com.poochie.domain.segmentation.age;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
public class AgeSegmentationPercentage {

  @Id
  @GeneratedValue
  private Long id;

  @Valid
  @NotNull
  @OneToOne(cascade = CascadeType.ALL)
  private BoyPercentage boyPercentage;

  @Valid
  @NotNull
  @OneToOne(cascade = CascadeType.ALL)
  private PreTeenPercentage preTeenPercentage;

  @Valid
  @NotNull
  @OneToOne(cascade = CascadeType.ALL)
  private TeenPercentage teenPercentage;

  @Valid
  @NotNull
  @OneToOne(cascade = CascadeType.ALL)
  private PreAdultPercentage preAdultPercentage;

  @Valid
  @NotNull
  @OneToOne(cascade = CascadeType.ALL)
  private AdultPercentage adultPercentage;

  @Valid
  @NotNull
  @OneToOne(cascade = CascadeType.ALL)
  private AncientPercentage ancientPercentage;
}
