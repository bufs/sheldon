package com.poochie.domain.segmentation.age;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

/**
 * Created by tomaslingotti on 7/18/17.
 */
@Entity
public class BoyPercentage implements AgeDescriptor {

  public static final String NAME = "Boy";

  @Id
  @GeneratedValue
  @ApiModelProperty(hidden = true)
  private Long id;
  private Integer percentage;
  @NotNull
  private Integer quantity;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Integer getPercentage() {
    return percentage;
  }

  public void setPercentage(Integer percentage) {
    this.percentage = percentage;
  }

  @Override
  @Transient
  public String get() {
    return NAME;
  }

  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }
}
