package com.poochie.domain.segmentation.nse;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
@Setter
@Getter
public class HighNse implements NseSegmentationDescriptor {

  public static final String NAME = "High";

  @Id
  @GeneratedValue
  @ApiModelProperty(hidden = true)
  private Long id;

  private Integer percentage;

  private Integer quantity;

  @Override
  @Transient
  public String get() {
    return NAME;
  }
}

