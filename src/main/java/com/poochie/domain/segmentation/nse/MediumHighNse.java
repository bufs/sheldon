package com.poochie.domain.segmentation.nse;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class MediumHighNse implements NseSegmentationDescriptor {

  public static final String NAME = "MediumHigh";

  @Id
  @GeneratedValue
  @ApiModelProperty(hidden = true)
  private Long id;
  private Integer percentage;
  private Integer quantity;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Integer getPercentage() {
    return percentage;
  }

  public void setPercentage(Integer percentage) {
    this.percentage = percentage;
  }

  @Override
  @Transient
  public String get() {
    return NAME;
  }

  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }
}
