package com.poochie.domain.segmentation.nse;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
@Setter
@Getter
public class NseSegmentation {

  @Id
  @GeneratedValue
  private Long id;

  private Boolean emergency;

  private Boolean low;

  private Boolean mediumLow;

  private Boolean medium;

  private Boolean mediumHigh;

  private Boolean high;


  @OneToOne(cascade = CascadeType.ALL)
  private NseSegmentationPercentage percentage;

}
