package com.poochie.domain.segmentation.nse;

public interface NseSegmentationDescriptor {

  String get();

  Integer getQuantity();
}
