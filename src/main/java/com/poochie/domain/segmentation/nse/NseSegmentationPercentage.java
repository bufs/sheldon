package com.poochie.domain.segmentation.nse;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
@Getter
@Setter
public class NseSegmentationPercentage {

  @Id
  @GeneratedValue
  private Long id;

  @OneToOne(cascade = CascadeType.ALL)
  private EmergencyNse emergencyNse;

  @OneToOne(cascade = CascadeType.ALL)
  private LowNse lowNse;

  @OneToOne(cascade = CascadeType.ALL)
  private MediumLowNse mediumLowNse;

  @OneToOne(cascade = CascadeType.ALL)
  private MediumNse mediumNse;

  @OneToOne(cascade = CascadeType.ALL)
  private MediumHighNse mediumHighNse;

  @OneToOne(cascade = CascadeType.ALL)
  private HighNse highNse;
}
