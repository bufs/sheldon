package com.poochie.domain.segmentation.profession;

import com.poochie.domain.segmentation.Area;
import com.poochie.domain.company.CompanyActivity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
@Getter
@Setter
public class ProfessionSegmentation {

  @Id
  @GeneratedValue
  private Long id;

  @OneToOne
  private CompanyActivity companySection;

  @OneToOne
  private Area area;

  private String position;
}
