package com.poochie.domain.segmentation.sex;

import com.poochie.domain.segmentation.age.AgeDescriptor;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
public class MalePercentage implements AgeDescriptor {

  public static final String NAME = "Male";

  @Id
  @GeneratedValue
  @ApiModelProperty(hidden = true)
  private Long id;
  @NotNull
  @Enumerated(value = EnumType.STRING)
  private Sex male;
  private Integer percentage;
  @NotNull
  private Integer quantity;

  @Override
  @Transient
  public String get() {
    return NAME;
  }
}
