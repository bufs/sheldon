package com.poochie.domain.segmentation.sex;

/**
 * Created by tomaslingotti on 7/18/17.
 */
public enum Sex {
  F, M
}
