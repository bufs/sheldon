package com.poochie.domain.segmentation.sex;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
@Getter
@Setter
public class SexSegmentation {

  @Id
  @GeneratedValue
  @ApiModelProperty(hidden = true)
  private Long id;

  private Boolean toM;

  private Boolean toF;

  @OneToOne(cascade = CascadeType.ALL)
  private SexSegmentationPercentage percentage;
}
