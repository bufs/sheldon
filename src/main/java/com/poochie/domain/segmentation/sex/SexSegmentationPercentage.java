package com.poochie.domain.segmentation.sex;

import com.poochie.domain.segmentation.sex.FemalePercentage;
import com.poochie.domain.segmentation.sex.MalePercentage;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
public class SexSegmentationPercentage {

  @Id
  @GeneratedValue
  private Long id;

  @Valid
  @NotNull
  @OneToOne(cascade = CascadeType.ALL)
  private FemalePercentage femalePercentage;

  @Valid
  @NotNull
  @OneToOne(cascade = CascadeType.ALL)
  private MalePercentage malePercentage;
}
