package com.poochie.domain.supporter;

import com.poochie.domain.company.Company;
import com.poochie.domain.coverage.Coverage;
import com.poochie.domain.coverage.Place;
import com.poochie.domain.segmentation.Segmentation;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Supporter {

  @Id
  @GeneratedValue
  private Long id;

  @Enumerated(value = EnumType.STRING)
  private SupporterType supporterType;

  @Enumerated(value = EnumType.STRING)
  private SupporterClassification classification;

  @OneToOne(cascade = CascadeType.ALL)
  private Coverage coverage;

  @OneToOne(cascade = CascadeType.ALL)
  private Place place;

  @OneToOne(cascade = CascadeType.ALL)
  private Segmentation segmentation;

  private Boolean illuminated;

  private String size;

  private String freq;

  private Double finalCost;

  @ManyToOne
  private Company company;
}
