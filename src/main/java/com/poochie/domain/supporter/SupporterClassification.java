package com.poochie.domain.supporter;

public enum SupporterClassification {
  NOTICIAS,
  ORGANIZACION,
  HACKATON,
  CONGRESOS,
  ECOMMERCE,
  GRILLA,
  PREDIO,
  NOTICIAS_AGRO,
  NOTICIAS_GENERALES
}
