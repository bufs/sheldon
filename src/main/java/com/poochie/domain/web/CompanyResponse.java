package com.poochie.domain.web;

import java.util.List;

public class CompanyResponse<T> {

  private List<T> companyObject;

  public CompanyResponse(List<T> companyObject) {
    this.companyObject = companyObject;
  }

  public List<T> getCompanyObject() {
    return companyObject;
  }

  public void setCompanyObject(List<T> companyObject) {
    this.companyObject = companyObject;
  }
}
