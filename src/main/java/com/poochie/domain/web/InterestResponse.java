package com.poochie.domain.web;

import com.poochie.domain.media.Interest;

import java.util.List;

/**
 * Created by tomas.lingotti on 26/07/17.
 */
public class InterestResponse {

  private final List<Interest> interests;

  public InterestResponse(List<Interest> interests) {
    this.interests = interests;
  }

  public List<Interest> getInterests() {
    return interests;
  }
}
