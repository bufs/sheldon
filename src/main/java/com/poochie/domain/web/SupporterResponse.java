package com.poochie.domain.web;

import java.util.Collection;

/**
 * Created by tomas.lingotti on 26/07/17.
 */
public class SupporterResponse<T> {

  private final Collection<T> supporterResponse;

  public SupporterResponse(Collection<T> supporterResponse) {
    this.supporterResponse = supporterResponse;
  }

  public Collection<T> getSupporterResponse() {
    return supporterResponse;
  }
}
