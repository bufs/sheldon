package com.poochie.global.documentation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

  @Bean
  public Docket productApi() {
    return new Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors
                    .basePackage("com.poochie.web"))
            .build()
            .apiInfo(metaData());
  }

  private static ApiInfo metaData() {
    return new ApiInfo("Sheldon-project",
            "Strategic advertisement management.",
            "1.0.SNAPSHOT",
            "GNU",
            getMyself(),
            "Apache License V 2.0",
            "https://www.apache.org/licenses/LICENSE-2.0",
            new ArrayList<>());
  }

  private static Contact getMyself() {
    return new Contact("Tomas F. Lingotti", "https:github.com/tomiok", "tomaslingotti@gmail.com");
  }
}
