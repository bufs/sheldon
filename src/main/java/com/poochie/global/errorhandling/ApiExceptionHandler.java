package com.poochie.global.errorhandling;

import javax.validation.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class ApiExceptionHandler {

  private static final Logger log = LoggerFactory.getLogger(ApiExceptionHandler.class);

  @ExceptionHandler(value = { EntityNotFoundException.class })
  @ResponseBody
  public ResponseEntity<HttpError> customAdvice_EntityNotFound(final EntityNotFoundException e) {
    return create(e, HttpStatus.NOT_FOUND);
  }

  private static ResponseEntity<HttpError> create(final Throwable e,
                                                  final HttpStatus status) {
    log.error("Excption during the request", e);
    return createHttpResponse(e, status);
  }

  private static ResponseEntity<HttpError> createHttpResponse(final Throwable e,
                                                              final HttpStatus status) {
    final HttpHeaders headers = new HttpHeaders();
    headers.add("content-type", "application/json");
    final HttpError apiError = HttpError.create(e.getMessage(), status);
    return new ResponseEntity<>(apiError, headers, apiError.getHttpStatus());
  }

  @ExceptionHandler(value = { ValidationException.class })
  @ResponseBody
  public ResponseEntity<HttpError> customAdvice_MessageNotUpdated(final ValidationException e) {
    return create(e, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(value = { NullPointerException.class })
  @ResponseBody
  public ResponseEntity<HttpError> advice_NullPointerException(final NullPointerException e) {
    return create(e, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(value = { IllegalArgumentException.class })
  @ResponseBody
  public ResponseEntity<HttpError> advice_IllegalArgument(final IllegalArgumentException e) {
    return create(e, HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
