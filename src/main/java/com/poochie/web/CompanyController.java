package com.poochie.web;

import com.poochie.domain.company.CompanyActivity;
import com.poochie.domain.company.CompanyGroup;
import com.poochie.domain.company.CompanySection;
import com.poochie.domain.web.CompanyResponse;
import com.poochie.domain.repositories.CompanyActivityRepository;
import com.poochie.domain.repositories.CompanyGroupRepository;
import com.poochie.domain.repositories.CompanySectionRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.poochie.web.utils.ResourcesUtils.JSON_CONTENT_TYPE;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static org.springframework.http.ResponseEntity.ok;

/**
 * Created by tomas.lingotti on 30/07/17.
 */
@RestController
@RequestMapping(value = "/company")
@Api(description = "Resource to receive all the info related to the segmentation."
        + " All tha data is Read only.")
public class CompanyController {

  private final CompanyActivityRepository companyActivityRepository;
  private final CompanyGroupRepository companyGroupRepository;
  private final CompanySectionRepository companySectionRepository;

  @Autowired
  public CompanyController(CompanyActivityRepository companyActivityRepository,
                           CompanyGroupRepository companyGroupRepository,
                           CompanySectionRepository companySectionRepository) {
    this.companyActivityRepository = companyActivityRepository;
    this.companyGroupRepository = companyGroupRepository;
    this.companySectionRepository = companySectionRepository;
  }

  @GetMapping(value = "/activities", produces = JSON_CONTENT_TYPE)
  @ApiOperation(value = "All the company activities.", produces = JSON_CONTENT_TYPE, httpMethod = "GET")
  @ApiResponses(value = {
          @ApiResponse(code = SC_OK, message = "")
  })
  public ResponseEntity getAllCompanyActivities() {
    final CompanyResponse<CompanyActivity> obj = new CompanyResponse<>(companyActivityRepository.findAll());
    return new ResponseEntity<>(obj, HttpStatus.OK);
  }

  @GetMapping(value = "/groups", produces = JSON_CONTENT_TYPE)
  @ApiOperation(value = "All the company groups.", produces = JSON_CONTENT_TYPE, httpMethod = "GET")
  @ApiResponses(value = {
          @ApiResponse(code = SC_OK, message = "")
  })
  public ResponseEntity<List<CompanyGroup>> getAllCompanyGroups() {
    return ok(companyGroupRepository.findAll());

  }

  @GetMapping(value = "/sections", produces = JSON_CONTENT_TYPE)
  @ApiOperation(value = "All the company sections.", produces = JSON_CONTENT_TYPE, httpMethod = "GET")
  @ApiResponses(value = {
          @ApiResponse(code = SC_OK, message = "")
  })
  public ResponseEntity<List<CompanySection>> getAllCompanySections() {
    return ok((companySectionRepository.findAll()));
  }
}
