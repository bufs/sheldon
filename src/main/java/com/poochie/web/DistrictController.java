package com.poochie.web;

import com.poochie.component.district.DistrictComponent;
import com.poochie.domain.districts.City;
import com.poochie.domain.districts.Country;
import com.poochie.domain.districts.Province;
import com.poochie.component.district.DistrictComponentImpl;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/district")
public class DistrictController {

  private final DistrictComponent districtComponent;

  @Autowired
  public DistrictController(final DistrictComponentImpl districtComponent) {
    this.districtComponent = districtComponent;
  }

  @GetMapping(path = "/cities")
  @ApiOperation(value = "Get all the cities given a name and a province ID.")
  public ResponseEntity<List<City>> getAllCitiesByNameAndProvinceId(@RequestParam("name") final String name,
                                                                    @RequestParam("provinceId") final long provinceId) {
    return ResponseEntity.ok(districtComponent.getAllCitiesByNameAndProvince(name, provinceId));
  }

  @GetMapping(path = "/provinces")
  @ApiOperation(value = "All the provinces by name.")
  public ResponseEntity<List<Province>> getAllProvincesByName(@RequestParam("name") final String name) {
    return ResponseEntity.ok(districtComponent.getAllProvincesByName(name));
  }

  @GetMapping(path = "/countries")
  @ApiOperation(value = "All the countries by name.")
  public ResponseEntity<List<Country>> getAllCountries() {
    return ResponseEntity.ok(districtComponent.getAllCountries());
  }

}
