package com.poochie.web;

import com.poochie.domain.media.Interest;
import com.poochie.domain.web.InterestResponse;
import com.poochie.domain.repositories.InterestRepository;
import com.poochie.global.validator.ApiValidator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.poochie.web.utils.ResourcesUtils.JSON_CONTENT_TYPE;
import static javax.servlet.http.HttpServletResponse.SC_CREATED;
import static javax.servlet.http.HttpServletResponse.SC_OK;

@RestController
@RequestMapping(path = "/interests")
@Api(description = "CRUD of interests. The UI will catch like tags, Interests can be added via API.")
public class InterestController {

  private final InterestRepository interestRepository;
  private final ApiValidator validator;

  @Autowired
  public InterestController(InterestRepository interestRepository, ApiValidator validator) {
    this.interestRepository = interestRepository;
    this.validator = validator;
  }

  @PostMapping(consumes = JSON_CONTENT_TYPE)
  @ApiOperation(value = "Add an interest.", produces = JSON_CONTENT_TYPE,
          code = SC_CREATED)
  @ApiResponses(value = {
          @ApiResponse(code = SC_CREATED, message = "Interest saved OK.")
  })
  public ResponseEntity<Interest> addNewInterest(@RequestBody @ApiParam(required = true) final Interest interest) {
    validator.validate(interest);
    return new ResponseEntity<>(interestRepository.save(interest), HttpStatus.CREATED);
  }

  @GetMapping(produces = JSON_CONTENT_TYPE)
  @ApiOperation(value = "Get all the interests.", produces = JSON_CONTENT_TYPE,
          response = InterestResponse.class)
  @ApiResponses(value = {
          @ApiResponse(code = SC_OK, message = "See all the interests.")
  })
  public ResponseEntity<InterestResponse> getAllInterests() {
    return new ResponseEntity<>(new InterestResponse(interestRepository.findAll()), HttpStatus.OK);
  }
}
