package com.poochie.web;

import static com.poochie.web.utils.ResourcesUtils.JSON_CONTENT_TYPE;
import static org.springframework.http.ResponseEntity.ok;

import com.poochie.component.filter.DynamicFilter;
import com.poochie.component.filter.FilterComponent;
import com.poochie.component.outdoor.MediaComponent;
import com.poochie.domain.districts.Ubiety;
import com.poochie.domain.outdoor.OutdoorMedia;
import com.poochie.domain.repositories.SupporterRepository;
import com.poochie.domain.supporter.Supporter;
import com.poochie.domain.supporter.SupporterClassification;
import com.poochie.domain.supporter.SupporterType;
import com.poochie.domain.web.SupporterResponse;
import com.poochie.web.fileprocessors.AbstractBaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/supporters")
@Api(description = "Get all the supporters, types and classification. Also get supporters by user id (the user who"
                   + " created the supporter via Excel file upload."
                   + "\n Also the supporter update can be performed here.")
public class SupporterController extends AbstractBaseController {

  private final SupporterRepository supporterRepository;

  private final MediaComponent mediaComponent;

  private final FilterComponent filterComponent;

  @Autowired
  public SupporterController(final SupporterRepository supporterRepository,
                             final MediaComponent mediaComponent,
                             final FilterComponent filterComponent) {
    this.supporterRepository = supporterRepository;
    this.mediaComponent = mediaComponent;
    this.filterComponent = filterComponent;
  }

  @GetMapping(path = "/types", produces = JSON_CONTENT_TYPE)
  @ApiOperation(value = "Get all the supporter types."
      , response = SupporterResponse.class)
  public ResponseEntity<String> getSupporterTypes() {
    return ok(convertToString(EnumSet.allOf(SupporterType.class)));
  }

  @GetMapping(path = "/classification")
  @ApiOperation(value = "Get all the supporter classifications.", produces = JSON_CONTENT_TYPE, response =
      SupporterResponse.class)
  public ResponseEntity<String> getSupporterClassification() {
    return ok(convertToString(EnumSet.allOf(SupporterClassification.class)));
  }

  @GetMapping
  public ResponseEntity<List<Supporter>> getSupportersByUserId(@RequestParam("userId") final long userId) {
    List<Supporter> supporters =
        mediaComponent.findAllByUserId(userId)
            .stream()
            .map(OutdoorMedia::getSupporter)
            .collect(Collectors.toList());

    return ok(supporters);
  }

  @PutMapping
  @ApiOperation(value = "Update the supporter, add the ID in the request body.")
  public ResponseEntity<Supporter> updateSupporter(@RequestBody final Supporter supporter) {
    return ok(supporterRepository.save(supporter));
  }

  @GetMapping("/search/")
  public ResponseEntity<List<Supporter>> findSupportersByCriteria(@RequestParam("userId") final long userId,
                                                                  @RequestParam("ubiety") final Ubiety ubiety,
                                                                  @RequestParam("illuminated") final boolean
                                                                      illuminated) {
    return ok(filterComponent.findSupportersByCriteria(userId, new DynamicFilter(ubiety, illuminated)));
  }
}
