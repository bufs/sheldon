package com.poochie.web.commons;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseController {

  @Autowired
  private Gson gson;

  protected <T> String toJsonString(final T t) {
    return gson.toJson(t);
  }
}
