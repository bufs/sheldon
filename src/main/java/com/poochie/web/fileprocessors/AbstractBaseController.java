package com.poochie.web.fileprocessors;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractBaseController {

  @Autowired
  private Gson gson;

  protected <S> String convertToString(S object) {
    return gson.toJson(object);
  }
}
