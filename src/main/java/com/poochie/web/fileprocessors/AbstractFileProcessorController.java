package com.poochie.web.fileprocessors;

import static java.util.stream.Collectors.toList;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

public abstract class AbstractFileProcessorController<T> extends AbstractBaseController {

  T sheet;

  Iterator<Row> createRowIterator(final MultipartFile multipartFile, final int index) throws Exception {
    Workbook workbook = new XSSFWorkbook(multipartFile.getInputStream());
    Sheet sheet = workbook.getSheetAt(index);
    return sheet.rowIterator();
  }

  protected abstract List<T> read(final Iterator<Row> rowIterator);

  List<String> unwrapToListString(final Row row, final int index) {
    String stringValue = unwrapToString(row, index);
    return Arrays.stream(stringValue.split(",")).collect(toList());
  }

  String unwrapToString(final Row row, final int index) {
    return row.getCell(index).getStringCellValue();
  }

  boolean unwrapToBoolean(final Row row, final int index) {
    String stringValue = unwrapToString(row, index);
    return stringValue.equalsIgnoreCase("si");
  }

  double unwrapToNumeric(final Row row, final int index) {
    return row.getCell(index).getNumericCellValue();
  }
}
