package com.poochie.web.fileprocessors;

import com.poochie.domain.excel.OutdoorSheet;
import com.poochie.domain.media.Interest;
import org.apache.poi.ss.usermodel.Row;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class OutdoorFileProcessor extends AbstractFileProcessorController<OutdoorSheet> {

  @Override
  protected List<OutdoorSheet> read(final Iterator<Row> rowIterator) {
    List<OutdoorSheet> outdoorEntries = new ArrayList<>();
    while (rowIterator.hasNext()) {

      sheet = new OutdoorSheet();
      Row row = rowIterator.next();
      if (row.getRowNum() == 0) {
        continue;
      }
      if (row.getCell(0) == null) {
        break;
      }

      sheet.setMediaId(unwrapToString(row, 0));
      sheet.setUrlToPublish(unwrapToString(row, 1));
      sheet.setSupporter(unwrapToString(row, 2));
      sheet.setSize(unwrapToString(row, 3));
      sheet.setImageUri(unwrapToString(row, 4));
      sheet.setVideoUri(unwrapToString(row, 5));
      sheet.setIlluminated(unwrapToBoolean(row, 6));
      sheet.setFreq(unwrapToString(row, 7));
      sheet.setCoverage(unwrapToNumeric(row, 8));
      sheet.setCoverageUrl(unwrapToString(row, 9));
      sheet.setSexSegmentation(unwrapToString(row, 10));
      sheet.setAgeSegmentation(unwrapToString(row, 11));
      sheet.setNseSegmentation(unwrapToString(row, 12));
      sheet.setSector(unwrapToString(row, 13));
      sheet.setArea(unwrapToString(row, 14));
      sheet.setPosition(unwrapToString(row, 15));
      sheet.setInterests(unwrapToListString(row, 16)
              .stream()
              .map(Interest::new)
              .collect(Collectors.toList()));
      sheet.setFinalCost(unwrapToNumeric(row, 17));

      outdoorEntries.add(sheet);
    }

    return outdoorEntries;
  }

}
