package com.poochie.web.fileprocessors;

import com.poochie.domain.excel.TvAdvertisingSheet;
import com.poochie.domain.excel.TvSheet;
import org.apache.poi.ss.usermodel.Row;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public abstract class TvFileProcessor extends AbstractFileProcessorController<TvSheet> {

  @Override
  protected List<TvSheet> read(final Iterator<Row> rowIterator) {
    List<TvSheet> tvEntries = new ArrayList<>();

    while (rowIterator.hasNext()) {
      sheet = new TvSheet();
      Row row = rowIterator.next();
      if (row.getRowNum() == 0) {
        continue;
      }
      if (row.getCell(0) == null) {
        break;
      }

      sheet.setMediaId(unwrapToString(row, 0));
      sheet.setProgramName(unwrapToString(row, 1));
      sheet.setProgramType(unwrapToString(row, 2));
      sheet.setImageUri(unwrapToString(row, 3));
      sheet.setVideoUri(unwrapToString(row, 4));
      sheet.setStartTime(unwrapToNumeric(row, 5));
      sheet.setEndTime(unwrapToNumeric(row, 6));
      sheet.setDays(unwrapToListString(row, 7));
      sheet.setFreq(unwrapToString(row, 8));
      sheet.setCoverage(unwrapToNumeric(row, 9));
      sheet.setUriCoverageLink(unwrapToString(row, 10));
      sheet.setSexSegmentation(unwrapToString(row, 11));
      sheet.setAgeSegmentation(unwrapToString(row, 12));
      sheet.setNseSegmentation(unwrapToString(row, 13));
      sheet.setSector(unwrapToString(row, 14));
      sheet.setArea(unwrapToString(row, 15));
      sheet.setPosition(unwrapToString(row, 16));
      sheet.setInterests(unwrapToListString(row, 17));

      tvEntries.add(sheet);
    }

    return tvEntries;
  }

  protected List<TvSheet> readAdvertisingSpaces(final Iterator<Row> rowIterator, final List<TvSheet> tvEntries) {

    tvEntries.forEach(tv -> {
      Set<TvAdvertisingSheet> tvAdvertisingSheets = new HashSet<>();

      while (rowIterator.hasNext()) {
        TvAdvertisingSheet adsSheet = new TvAdvertisingSheet();

        Row row = rowIterator.next();
        if (row.getRowNum() == 0) {
          continue;
        }
        if (row.getCell(0) == null) {
          break;
        }

        adsSheet.setMediaId(unwrapToString(row, 0));
        adsSheet.setAdvertisingSpace(unwrapToString(row, 1));
        adsSheet.setSizeInPx(unwrapToString(row, 2));
        adsSheet.setImageUri(unwrapToString(row, 3));
        adsSheet.setVideoUri(unwrapToString(row, 4));
        adsSheet.setFinalCostBySecond(unwrapToNumeric(row, 5));
        // Add the Advertisement to the set.
        tvAdvertisingSheets.add(adsSheet);
      }
      //Add the set of advertisements to the Program.
      tv.setTvAdvertisingSheets(tvAdvertisingSheets);
    });

    return tvEntries;
  }
}
