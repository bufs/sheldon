package com.poochie.web.fileprocessors;

import com.poochie.component.outdoor.MediaComponent;
import com.poochie.domain.excel.OutdoorSheet;
import com.poochie.domain.outdoor.OutdoorMedia;
import com.poochie.domain.repositories.UserRepository;
import com.poochie.component.outdoor.MediaComponentImpl;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Iterator;
import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/submit/media")
public class UploadOutdoorFileController extends OutdoorFileProcessor {

  private final MediaComponent mediaComponent;

  private final UserRepository userRepository;

  @Autowired
  public UploadOutdoorFileController(final MediaComponentImpl mediaComponent,
                                     final UserRepository userRepository) {
    this.mediaComponent = mediaComponent;
    this.userRepository = userRepository;
  }

  private static final int INDEX = 0;

  @PostMapping(path = "/outdoor")
  public ResponseEntity<List<OutdoorMedia>> uploadOutdoorMedia(@RequestParam("file") final MultipartFile multipartFile,
                                                               @RequestParam("userId") final long userId)
          throws Exception {
    Iterator<Row> rowIterator = createRowIterator(multipartFile, INDEX);
    List<OutdoorSheet> sheet = read(rowIterator);

    sheet.forEach(outdoorSheet -> outdoorSheet.setUser(userRepository.findOne(userId)));
    return ok(mediaComponent.persistMedia(sheet));
  }
}