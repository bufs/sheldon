package com.poochie.web.fileprocessors;

import com.poochie.domain.excel.TvSheet;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Iterator;
import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping(path = "/submit/media")
public class UploadTvFileController extends TvFileProcessor {

  private static final int FIRST_SHEET_INDEX = 0;
  private static final int SECOND_SHEET_INDEX = 1;

  @PostMapping(path = "/tv")
  public ResponseEntity<String> uploadTvMedia(@RequestParam("file") final MultipartFile multipartFile)
          throws Exception {

    Iterator<Row> rowIterator = createRowIterator(multipartFile, FIRST_SHEET_INDEX);
    List<TvSheet> tvSheets = read(rowIterator);

    Iterator<Row> adsRowIterator = createRowIterator(multipartFile, SECOND_SHEET_INDEX);
    return ok(convertToString(readAdvertisingSpaces(adsRowIterator, tvSheets)));
  }
}
