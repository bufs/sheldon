package com.poochie.web.user;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthenticationCredentials {

  private String email;

  private String password;
}
