package com.poochie.web.user;

import com.poochie.domain.User;
import com.poochie.domain.repositories.UserRepository;
import com.poochie.global.validator.ApiValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping(path = "/users")
public class UserController {

  private final UserRepository userRepository;
  private final ApiValidator apiValidator;

  @Autowired
  public UserController(final UserRepository userRepository,
                        final ApiValidator apiValidator) {
    this.userRepository = userRepository;
    this.apiValidator = apiValidator;
  }

  @PostMapping
  public ResponseEntity<UserHttpResponse> addUser(@RequestBody final UserHttpRequest userHttpRequest) {
    apiValidator.validate(userHttpRequest);
    User user = UserMapper.toDomainObject(userHttpRequest);
    //TODO encrypt the pass here


    User savedUser = userRepository.save(user);
    return ok(new UserHttpResponse(savedUser));
  }

  @GetMapping
  public ResponseEntity<UserHttpResponse> findUserById(@RequestParam("userId") final long userId) {
    return ok(new UserHttpResponse(userRepository.findOne(userId)));
  }

  @PostMapping(path = "/authenticate")
  public ResponseEntity<UserHttpResponse> authenticateUser(@RequestBody final AuthenticationCredentials credentials) {
    User user = userRepository.findUserByEmailAndPassword(credentials.getEmail(), credentials.getPassword())
        .orElseThrow(() -> new RuntimeException("The user or the password are wrong."));

    return ok(new UserHttpResponse(user));
  }
}
