package com.poochie.web.user;

import com.poochie.domain.Job;
import com.poochie.domain.Role;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class UserHttpRequest {

  @NotNull(message = "The name cannot be null")
  private String name;

  @NotNull(message = "The last name cannot be null")
  private String lastName;

  @NotNull(message = "The email cannot be null")
  @Email
  private String email;

  @Size(min = 4, max = 16)
  @NotNull
  private String password;

  private Role role;

  private Job job;
}
