package com.poochie.web.user;

import com.poochie.domain.Job;
import com.poochie.domain.Role;
import com.poochie.domain.User;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserHttpResponse {

  private long id;

  private String name;

  private String lastName;

  private String email;

  private Role role;

  private Job job;

  public UserHttpResponse(final User user) {
    this.id = user.getId();
    this.name = user.getName();
    this.lastName = user.getLastName();
    this.email = user.getEmail();
    this.role = user.getRole();
    this.job = user.getJob();
  }
}
