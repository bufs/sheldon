package com.poochie.web.user;

import com.poochie.domain.User;

public class UserMapper {

  public static User toDomainObject(final UserHttpRequest request) {
    return new User(request.getName(), request.getLastName(), request.getEmail(),
            request.getPassword(), request.getRole(), request.getJob());
  }
}
