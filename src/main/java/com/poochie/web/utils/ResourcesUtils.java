package com.poochie.web.utils;

import org.springframework.http.MediaType;

/**
 * Created by tomaslingotti on 7/19/17.
 */
public class ResourcesUtils {

  public static final String MEDIA_BASE_PATH = "media";
  public static final String COMPANY_BASE_PATH = "company";
  public static final String SUPPORTER_BASE_PATH = "supporter";
  public static final String SUPPORTER_TYPE_PATH = "type";
  public static final String SUPPORTER_CLASSIFICATION_PATH = "classification";
  public static final String COMPANY_GROUP_PATH = "group";
  public static final String COMPANY_SECTION_PATH = "section";
  public static final String COMPANY_ACTIVITY_PATH = "activity";
  public static final String INTEREST_BASE_PATH = "interest";
  public static final String JSON_CONTENT_TYPE = MediaType.APPLICATION_JSON_VALUE;
  public static final String SLASH = "/";
  public static final String GET_ALL = "get-all";
  public static final String ADD = "add";

  private ResourcesUtils() {
    throw new RuntimeException("Never instantiate this class....");
  }
}
