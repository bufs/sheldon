CREATE TABLE adult_percentage (
  id         BIGINT NOT NULL AUTO_INCREMENT,
  percentage INTEGER,
  quantity   INTEGER,
  PRIMARY KEY (id)
);

CREATE TABLE age_segmentation (
  id                      BIGINT NOT NULL AUTO_INCREMENT,
  adult_percentage_id     BIGINT NOT NULL,
  ancient_percentage_id   BIGINT NOT NULL,
  boy_percentage_id       BIGINT NOT NULL,
  pre_adult_percentage_id BIGINT NOT NULL,
  pre_teen_percentage_id  BIGINT NOT NULL,
  teen_percentage_id      BIGINT NOT NULL,
  PRIMARY KEY (id)
);
CREATE TABLE ancient_percentage (
  id         BIGINT NOT NULL AUTO_INCREMENT,
  percentage INTEGER,
  quantity   INTEGER,
  PRIMARY KEY (id)
);
CREATE TABLE area (
  id        BIGINT NOT NULL,
  area      VARCHAR(255),
  hierarchy INTEGER,
  PRIMARY KEY (id)
);
CREATE TABLE arrange (
  id    BIGINT NOT NULL AUTO_INCREMENT,
  day   VARCHAR(255),
  end   TIME,
  start TIME,
  PRIMARY KEY (id)
);
CREATE TABLE boy_percentage (
  id         BIGINT  NOT NULL AUTO_INCREMENT,
  percentage INTEGER,
  quantity   INTEGER NOT NULL,
  PRIMARY KEY (id)
);
CREATE TABLE cities (
  id          BIGINT NOT NULL,
  name        VARCHAR(255),
  postal_code VARCHAR(255),
  province_id BIGINT,
  PRIMARY KEY (id)
);
CREATE TABLE company (
  id                BIGINT       NOT NULL AUTO_INCREMENT,
  name              VARCHAR(255) NOT NULL,
  url               VARCHAR(255) NOT NULL,
  web_group         VARCHAR(255),
  contact_id        BIGINT,
  second_contact_id BIGINT,
  supporter_id      BIGINT       NOT NULL,
  PRIMARY KEY (id)
);
CREATE TABLE company_activity (
  id               BIGINT NOT NULL AUTO_INCREMENT,
  code             VARCHAR(255),
  name             VARCHAR(255),
  company_group_id BIGINT,
  PRIMARY KEY (id)
);
CREATE TABLE company_group (
  id                 BIGINT NOT NULL AUTO_INCREMENT,
  name               VARCHAR(255),
  company_section_id BIGINT,
  PRIMARY KEY (id)
);
CREATE TABLE company_section (
  id   BIGINT NOT NULL AUTO_INCREMENT,
  name VARCHAR(255),
  PRIMARY KEY (id)
);
CREATE TABLE contact (
  id           BIGINT       NOT NULL AUTO_INCREMENT,
  contact      VARCHAR(255),
  country_code VARCHAR(255),
  email        VARCHAR(255) NOT NULL,
  phone        VARCHAR(255),
  PRIMARY KEY (id)
);
CREATE TABLE countries (
  id   BIGINT NOT NULL,
  code VARCHAR(255),
  name VARCHAR(255),
  PRIMARY KEY (id)
);
CREATE TABLE coverage (
  id            BIGINT NOT NULL AUTO_INCREMENT,
  coverage_type VARCHAR(255),
  PRIMARY KEY (id)
);
CREATE TABLE coverage_arranges (
  coverage_id BIGINT NOT NULL,
  arranges_id BIGINT NOT NULL
);
CREATE TABLE emergency_nse (
  id         BIGINT NOT NULL AUTO_INCREMENT,
  percentage INTEGER,
  quantity   INTEGER,
  PRIMARY KEY (id)
);
CREATE TABLE female_percentage (
  id         BIGINT       NOT NULL AUTO_INCREMENT,
  female     VARCHAR(255) NOT NULL,
  percentage INTEGER,
  quantity   INTEGER      NOT NULL,
  PRIMARY KEY (id)
);
CREATE TABLE high_nse (
  id         BIGINT NOT NULL AUTO_INCREMENT,
  percentage INTEGER,
  quantity   INTEGER,
  PRIMARY KEY (id)
);
CREATE TABLE interest (
  id               BIGINT NOT NULL AUTO_INCREMENT,
  interest         VARCHAR(255),
  interest_type_id BIGINT,
  PRIMARY KEY (id)
);
CREATE TABLE interest_type (
  id            BIGINT NOT NULL AUTO_INCREMENT,
  interest_type VARCHAR(255),
  PRIMARY KEY (id)
);
CREATE TABLE job (
  id        BIGINT NOT NULL AUTO_INCREMENT,
  hierarchy VARCHAR(255),
  job       VARCHAR(255),
  PRIMARY KEY (id)
);
CREATE TABLE location (
  id           BIGINT NOT NULL AUTO_INCREMENT,
  address      VARCHAR(255),
  latitude     VARCHAR(255),
  location     VARCHAR(255),
  longitude    VARCHAR(255),
  min_circuits BIGINT,
  number       VARCHAR(255),
  place        VARCHAR(255),
  ubiety       VARCHAR(255),
  PRIMARY KEY (id)
);
CREATE TABLE low_nse (
  id         BIGINT NOT NULL AUTO_INCREMENT,
  percentage INTEGER,
  quantity   INTEGER,
  PRIMARY KEY (id)
);
CREATE TABLE male_percentage (
  id         BIGINT       NOT NULL AUTO_INCREMENT,
  male       VARCHAR(255) NOT NULL,
  percentage INTEGER,
  quantity   INTEGER      NOT NULL,
  PRIMARY KEY (id)
);
CREATE TABLE media_request (
  id         BIGINT NOT NULL AUTO_INCREMENT,
  company_id BIGINT,
  PRIMARY KEY (id)
);
CREATE TABLE medium_high_nse (
  id         BIGINT NOT NULL AUTO_INCREMENT,
  percentage INTEGER,
  quantity   INTEGER,
  PRIMARY KEY (id)
);
CREATE TABLE medium_low_nse (
  id         BIGINT NOT NULL AUTO_INCREMENT,
  percentage INTEGER,
  quantity   INTEGER,
  PRIMARY KEY (id)
);
CREATE TABLE medium_nse (
  id         BIGINT NOT NULL AUTO_INCREMENT,
  percentage INTEGER,
  quantity   INTEGER,
  PRIMARY KEY (id)
);
CREATE TABLE nse_segmentation (
  id                 BIGINT NOT NULL AUTO_INCREMENT,
  emergency_nse_id   BIGINT,
  high_nse_id        BIGINT,
  low_nse_id         BIGINT,
  medium_high_nse_id BIGINT,
  medium_low_nse_id  BIGINT,
  medium_nse_id      BIGINT,
  PRIMARY KEY (id)
);
CREATE TABLE place (
  id      BIGINT NOT NULL AUTO_INCREMENT,
  city    VARCHAR(255),
  country VARCHAR(255),
  other   VARCHAR(255),
  PRIMARY KEY (id)
);
CREATE TABLE population_city (
  id          BIGINT NOT NULL AUTO_INCREMENT,
  age         INTEGER,
  last_update TINYBLOB,
  man         INTEGER,
  total       INTEGER,
  woman       INTEGER,
  city_id     BIGINT,
  PRIMARY KEY (id)
);
CREATE TABLE population_country (
  id          BIGINT NOT NULL AUTO_INCREMENT,
  age         INTEGER,
  last_update TINYBLOB,
  man         INTEGER,
  total       INTEGER,
  woman       INTEGER,
  country_id  BIGINT,
  PRIMARY KEY (id)
);
CREATE TABLE population_province (
  id          BIGINT NOT NULL AUTO_INCREMENT,
  age         INTEGER,
  last_update TINYBLOB,
  man         INTEGER,
  total       INTEGER,
  woman       INTEGER,
  province_id BIGINT,
  PRIMARY KEY (id)
);
CREATE TABLE pre_adult_percentage (
  id         BIGINT NOT NULL AUTO_INCREMENT,
  percentage INTEGER,
  quantity   INTEGER,
  PRIMARY KEY (id)
);
CREATE TABLE pre_teen_percentage (
  id         BIGINT  NOT NULL AUTO_INCREMENT,
  percentage INTEGER,
  quantity   INTEGER NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE profession (
  id                 BIGINT NOT NULL AUTO_INCREMENT,
  position           VARCHAR(255),
  area_id            BIGINT,
  company_section_id BIGINT,
  PRIMARY KEY (id)
);
CREATE TABLE profession_division (
  id         BIGINT NOT NULL AUTO_INCREMENT,
  accounters DOUBLE PRECISION,
  doctors    DOUBLE PRECISION,
  engineer   DOUBLE PRECISION,
  lawyers    DOUBLE PRECISION,
  students   DOUBLE PRECISION,
  PRIMARY KEY (id)
);
CREATE TABLE provinces (
  id         BIGINT NOT NULL,
  name       VARCHAR(255),
  country_id BIGINT,
  PRIMARY KEY (id)
);
CREATE TABLE ranking (
  id        BIGINT NOT NULL AUTO_INCREMENT,
  cpm       DOUBLE PRECISION,
  cobertura DOUBLE PRECISION,
  frequency DOUBLE PRECISION,
  impact    DOUBLE PRECISION,
  PRIMARY KEY (id)
);
CREATE TABLE segmentation (
  id                  BIGINT NOT NULL AUTO_INCREMENT,
  age_segmentation_id BIGINT NOT NULL,
  nse_segmentation_id BIGINT,
  profession_id       BIGINT,
  sex_segmentation_id BIGINT,
  PRIMARY KEY (id)
);
CREATE TABLE segmentation_interests (
  segmentation_id BIGINT NOT NULL,
  interests_id    BIGINT NOT NULL
);
CREATE TABLE sex_segmentation (
  id                   BIGINT NOT NULL AUTO_INCREMENT,
  female_percentage_id BIGINT NOT NULL,
  male_percentage_id   BIGINT NOT NULL,
  PRIMARY KEY (id)
);
CREATE TABLE sub_interest (
  id           BIGINT NOT NULL AUTO_INCREMENT,
  sub_interest VARCHAR(255),
  interest_id  BIGINT,
  PRIMARY KEY (id)
);
CREATE TABLE supporter (
  id              BIGINT       NOT NULL AUTO_INCREMENT,
  classification  VARCHAR(255) NOT NULL,
  supporter_type  VARCHAR(255) NOT NULL,
  coverage_id     BIGINT,
  place_id        BIGINT,
  segmentation_id BIGINT       NOT NULL,
  PRIMARY KEY (id)
);
CREATE TABLE tax (
  id         BIGINT NOT NULL AUTO_INCREMENT,
  municipal  BIT,
  national   BIT,
  provincial BIT,
  PRIMARY KEY (id)
);
CREATE TABLE teen_percentage (
  id         BIGINT  NOT NULL AUTO_INCREMENT,
  percentage INTEGER,
  quantity   INTEGER NOT NULL,
  PRIMARY KEY (id)
);
CREATE TABLE user (
  id        BIGINT NOT NULL AUTO_INCREMENT,
  email     VARCHAR(255),
  last_name VARCHAR(255),
  name      VARCHAR(255),
  password  VARCHAR(255),
  role      INTEGER,
  job_id    BIGINT,
  PRIMARY KEY (id)
);
ALTER TABLE coverage_arranges
  ADD CONSTRAINT UK_6y25056mtiuwkmjc494j8c00o UNIQUE (arranges_id);
ALTER TABLE segmentation_interests
  ADD CONSTRAINT UK_9bxhd56ygol1kevd8slwujsa6 UNIQUE (interests_id);
ALTER TABLE age_segmentation
  ADD CONSTRAINT FK3b8qehabvaltqnr2m8ntr8tqu FOREIGN KEY (adult_percentage_id) REFERENCES adult_percentage (id);
ALTER TABLE age_segmentation
  ADD CONSTRAINT FKjjytcot2mgjhafodioakdsdgg FOREIGN KEY (ancient_percentage_id) REFERENCES ancient_percentage (id);
ALTER TABLE age_segmentation
  ADD CONSTRAINT FKldiquqp8fto4subvvum14lun6 FOREIGN KEY (boy_percentage_id) REFERENCES boy_percentage (id);
ALTER TABLE age_segmentation
  ADD CONSTRAINT FKql3vrk8jpqebwsceob3jy88hp FOREIGN KEY (pre_adult_percentage_id) REFERENCES pre_adult_percentage (id);
ALTER TABLE age_segmentation
  ADD CONSTRAINT FKjwdd1key08qxroo1s3m5uwvyb FOREIGN KEY (pre_teen_percentage_id) REFERENCES pre_teen_percentage (id);
ALTER TABLE age_segmentation
  ADD CONSTRAINT FKs3idtgv7yaacx407gnhdfuc3d FOREIGN KEY (teen_percentage_id) REFERENCES teen_percentage (id);
ALTER TABLE cities
  ADD CONSTRAINT FKcf2ndxcsekl26rrkb9egbhq20 FOREIGN KEY (province_id) REFERENCES provinces (id);
ALTER TABLE company
  ADD CONSTRAINT FK45nuqqimbkvrnvrb7877q62tm FOREIGN KEY (contact_id) REFERENCES contact (id);
ALTER TABLE company
  ADD CONSTRAINT FK4hoa7xrq3qay8d26narvtiw7t FOREIGN KEY (second_contact_id) REFERENCES contact (id);
ALTER TABLE company
  ADD CONSTRAINT FK60cv1xb8ybyqc94c42yen32by FOREIGN KEY (supporter_id) REFERENCES supporter (id);
ALTER TABLE company_activity
  ADD CONSTRAINT FKmbgcuwok8fqmtlhjrxmrwn1hi FOREIGN KEY (company_group_id) REFERENCES company_group (id);
ALTER TABLE company_group
  ADD CONSTRAINT FK1rsar7hl79fmtvf89dx441e23 FOREIGN KEY (company_section_id) REFERENCES company_section (id);
ALTER TABLE coverage_arranges
  ADD CONSTRAINT FKhv6eb1y419v8s84k5jtmd0mns FOREIGN KEY (arranges_id) REFERENCES arrange (id);
ALTER TABLE coverage_arranges
  ADD CONSTRAINT FK3bee2qhiobaks8qhld44f18k7 FOREIGN KEY (coverage_id) REFERENCES coverage (id);
ALTER TABLE interest
  ADD CONSTRAINT FKn2jbl9itw59e5g8c1yqnj45or FOREIGN KEY (interest_type_id) REFERENCES interest_type (id);
ALTER TABLE media_request
  ADD CONSTRAINT FK9wl340obudaa3h80rhjby0rwh FOREIGN KEY (company_id) REFERENCES company (id);
ALTER TABLE nse_segmentation
  ADD CONSTRAINT FKo51tqr05t63bl46t7y1xxk8vo FOREIGN KEY (emergency_nse_id) REFERENCES emergency_nse (id);
ALTER TABLE nse_segmentation
  ADD CONSTRAINT FK8jp77va8r8girqdsksms7jlfi FOREIGN KEY (high_nse_id) REFERENCES high_nse (id);
ALTER TABLE nse_segmentation
  ADD CONSTRAINT FK830a9keqsicirmtpmkm7cdfnf FOREIGN KEY (low_nse_id) REFERENCES low_nse (id);
ALTER TABLE nse_segmentation
  ADD CONSTRAINT FK5uu1j3e2v0mcsjcs709ods7b5 FOREIGN KEY (medium_high_nse_id) REFERENCES medium_high_nse (id);
ALTER TABLE nse_segmentation
  ADD CONSTRAINT FKgmyu66y6l6ae5ykn0naritgxn FOREIGN KEY (medium_low_nse_id) REFERENCES medium_low_nse (id);
ALTER TABLE nse_segmentation
  ADD CONSTRAINT FKk5dg5me6l8fckg26x53ojb2ao FOREIGN KEY (medium_nse_id) REFERENCES medium_nse (id);
ALTER TABLE population_city
  ADD CONSTRAINT FK9qqvlkxdwgnseuda0kvqeneuf FOREIGN KEY (city_id) REFERENCES cities (id);
ALTER TABLE population_country
  ADD CONSTRAINT FKi3p669r6se0kqws6d04y85vm2 FOREIGN KEY (country_id) REFERENCES countries (id);
ALTER TABLE population_province
  ADD CONSTRAINT FK9caclgocvj8enf2fvq3ijknkh FOREIGN KEY (province_id) REFERENCES provinces (id);
ALTER TABLE profession
  ADD CONSTRAINT FKsciaje5xb8xntbqdju5fsyhhg FOREIGN KEY (area_id) REFERENCES area (id);
ALTER TABLE profession
  ADD CONSTRAINT FK9imr4wfllwd52o3n6w1lkyx66 FOREIGN KEY (company_section_id) REFERENCES company_activity (id);
ALTER TABLE provinces
  ADD CONSTRAINT FK48p9qkti5auert2gquvn76338 FOREIGN KEY (country_id) REFERENCES countries (id);
ALTER TABLE segmentation
  ADD CONSTRAINT FKd22q9kr4pf69vcrtng9nclihd FOREIGN KEY (age_segmentation_id) REFERENCES age_segmentation (id);
ALTER TABLE segmentation
  ADD CONSTRAINT FKbqmc82bcb373pql3c6lh883yp FOREIGN KEY (nse_segmentation_id) REFERENCES nse_segmentation (id);
ALTER TABLE segmentation
  ADD CONSTRAINT FK2t7f5u4gnq82p10qk6tsdba98 FOREIGN KEY (profession_id) REFERENCES profession (id);
ALTER TABLE segmentation
  ADD CONSTRAINT FK4id0srk5w3w3b7lf0i8owvgud FOREIGN KEY (sex_segmentation_id) REFERENCES sex_segmentation (id);
ALTER TABLE segmentation_interests
  ADD CONSTRAINT FKk4f4faije1u9ffdbpsojucaaq FOREIGN KEY (interests_id) REFERENCES interest (id);
ALTER TABLE segmentation_interests
  ADD CONSTRAINT FKmrdhut36a1sipt7dbmo6ntql0 FOREIGN KEY (segmentation_id) REFERENCES segmentation (id);
ALTER TABLE sex_segmentation
  ADD CONSTRAINT FKin9ts6wn7i0rj3en7ell8udj0 FOREIGN KEY (female_percentage_id) REFERENCES female_percentage (id);
ALTER TABLE sex_segmentation
  ADD CONSTRAINT FKik0d73ktkng8cmu04okvakl2m FOREIGN KEY (male_percentage_id) REFERENCES male_percentage (id);
ALTER TABLE sub_interest
  ADD CONSTRAINT FK9rqqtvr0yrpdyine4h19710dg FOREIGN KEY (interest_id) REFERENCES interest (id);
ALTER TABLE supporter
  ADD CONSTRAINT FK6wr10uftid985120eehuuvwhp FOREIGN KEY (coverage_id) REFERENCES coverage (id);
ALTER TABLE supporter
  ADD CONSTRAINT FKpk3q5nj1r44wb5orcbenk178w FOREIGN KEY (place_id) REFERENCES place (id);
ALTER TABLE supporter
  ADD CONSTRAINT FKary0u9dcnkgr6bgcc28efx9wb FOREIGN KEY (segmentation_id) REFERENCES segmentation (id);
ALTER TABLE user
  ADD CONSTRAINT FKfftoc2abhot8f2wu6cl9a5iky FOREIGN KEY (job_id) REFERENCES job (id);
