package com.poochie.domain.media;

import com.poochie.domain.Contact;
import com.poochie.domain.company.Company;
import com.poochie.domain.districts.CountryCode;
import com.poochie.domain.segmentation.Segmentation;
import com.poochie.domain.segmentation.age.AdultPercentage;
import com.poochie.domain.segmentation.age.AgeSegmentation;
import com.poochie.domain.segmentation.age.AncientPercentage;
import com.poochie.domain.segmentation.age.BoyPercentage;
import com.poochie.domain.segmentation.age.PreAdultPercentage;
import com.poochie.domain.segmentation.age.PreTeenPercentage;
import com.poochie.domain.segmentation.age.TeenPercentage;
import com.poochie.domain.segmentation.nse.EmergencyNse;
import com.poochie.domain.segmentation.nse.HighNse;
import com.poochie.domain.segmentation.nse.LowNse;
import com.poochie.domain.segmentation.nse.MediumHighNse;
import com.poochie.domain.segmentation.nse.MediumLowNse;
import com.poochie.domain.segmentation.nse.MediumNse;
import com.poochie.domain.segmentation.nse.NseSegmentationPercentage;
import com.poochie.domain.segmentation.sex.FemalePercentage;
import com.poochie.domain.segmentation.sex.MalePercentage;
import com.poochie.domain.segmentation.sex.Sex;
import com.poochie.domain.segmentation.sex.SexSegmentation;
import com.poochie.domain.supporter.Supporter;
import com.poochie.domain.supporter.SupporterType;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.bind.annotation.RequestBody;

public class PercentageInjectorTest {

  private MediaRequest mediaRequest;
  private PercentageInjector injector;

  @Before
  public void setUp() {
    mediaRequest = new MediaRequestBuilder()
            .withSupporterType(SupporterType.TV)
            .withContactFields("Marcelo Tinelli", CountryCode.AR, "marcelo@showmatch.com", "01134239023302")
            .withMalePercentageQty(40)
            .withFemalePercentageQty(60)
            .withBoyPercentageQty(10)   //age
            .withPreTeenPercentageQty(10)
            .withTennPercentageQty(10)
            .withAncientPercentageQty(10)
            .withPreAdultPercentageQty(40)
            .withAdultPercentageQty(10)
            .withEmergencyNSEQty(10)    //nse
            .withLowNSEQty(10)
            .withMediumLowNSEQty(10)
            .withMediumNSEQty(10)
            .withMediumHighNSEQty(10)
            .withHighNSEQty(50)
            //.ensureCallSetters()
            .build();

    injector = new PercentageInjector();
  }

  @Test
  public void test() {

  }
/*
  @Test
  public void testMediaRequestNotNull() {
    Assert.assertNotNull(this.mediaRequest.getCompany().getSupporter());
  }

  @Test
  public void shouldReturn60_whenConstructMedia_given100people_and60Females() {
    MediaRequest _mediaRequest = injector.inject(mediaRequest);
    Assert.assertThat(_mediaRequest.getCompany().getSupporter().getSegmentation().getSexSegmentation().getFemalePercentage().getPercentage(), Is.is(60));
    Assert.assertThat(_mediaRequest.getCompany().getSupporter().getSegmentation().getSexSegmentation().getMalePercentage().getPercentage(), Is.is(40));
    Assert.assertThat(_mediaRequest.getCompany().getSupporter().getSegmentation().getNseSegmentationPercentage().getEmergencyNse().getPercentage(), Is.is(10));
    Assert.assertThat(_mediaRequest.getCompany().getSupporter().getSegmentation().getNseSegmentationPercentage().getHighNse().getPercentage(), Is.is(50));
  }

  @Test(expected = RuntimeException.class)
  public void shouldThrowRuntimeException() {
    this.mediaRequest.getCompany().getSupporter().getSegmentation().getNseSegmentationPercentage().getEmergencyNse().setQuantity(9999);
    injector.inject(mediaRequest);
    Assert.fail("Should not be here.");
  }*/

  private static class MediaRequestBuilder {
    private final MediaRequest mediaRequest = new MediaRequest();
    private final Company company = new Company();
    private final Contact contact = new Contact();
    private final Supporter supporter = new Supporter();
    private final Segmentation segmentation = new Segmentation();
    private final AgeSegmentation ageSegmentation = new AgeSegmentation();
    private final SexSegmentation sexSegmentation = new SexSegmentation();
    private final NseSegmentationPercentage nseSegmentationPercentage = new NseSegmentationPercentage();
    private final FemalePercentage femalePercentage = new FemalePercentage();
    private final MalePercentage malePercentage = new MalePercentage();
    private final BoyPercentage boyPercentage = new BoyPercentage();
    private final PreTeenPercentage preTeenPercentage = new PreTeenPercentage();
    private final TeenPercentage teenPercentage = new TeenPercentage();
    private final PreAdultPercentage preAdultPercentage = new PreAdultPercentage();
    private final AdultPercentage adultPercentage = new AdultPercentage();
    private final AncientPercentage ancientPercentage = new AncientPercentage();
    private final EmergencyNse emergencyNse = new EmergencyNse();
    private final LowNse lowNse = new LowNse();
    private final MediumLowNse mediumLowNse = new MediumLowNse();
    private final MediumNse mediumNse = new MediumNse();
    private final MediumHighNse mediumHighNse = new MediumHighNse();
    private final HighNse highNse = new HighNse();


    public MediaRequestBuilder withSupporterType(final SupporterType supporterType) {
      supporter.setSupporterType(supporterType);
      return this;
    }

    public MediaRequestBuilder withContactFields(final String contactField, final CountryCode countryCode,
                                                 final String email, final String phone) {
      contact.setContact(contactField);
      contact.setCountryCode(countryCode);
      contact.setEmail(email);
      contact.setPhone(phone);

      return this;
    }

        /*
            NseSegmentationPercentage
         */

    public MediaRequestBuilder withHighNSEQty(final Integer qty) {
      highNse.setQuantity(qty);
      return this;
    }

    public MediaRequestBuilder withMediumHighNSEQty(final Integer qty) {
      mediumHighNse.setQuantity(qty);
      return this;
    }

    public MediaRequestBuilder withMediumNSEQty(final Integer qty) {
      mediumNse.setQuantity(qty);
      return this;
    }

    public MediaRequestBuilder withMediumLowNSEQty(final Integer qty) {
      mediumLowNse.setQuantity(qty);
      return this;
    }

    public MediaRequestBuilder withLowNSEQty(final Integer qty) {
      lowNse.setQuantity(qty);
      return this;
    }

    public MediaRequestBuilder withEmergencyNSEQty(final Integer qty) {
      emergencyNse.setQuantity(qty);
      return this;
    }

        /*
            AGE
         */

    public MediaRequestBuilder withAncientPercentageQty(final Integer qty) {
      ancientPercentage.setQuantity(qty);
      return this;
    }

    public MediaRequestBuilder withAdultPercentageQty(final Integer qty) {
      adultPercentage.setQuantity(qty);
      return this;
    }

    public MediaRequestBuilder withPreAdultPercentageQty(final Integer qty) {
      preAdultPercentage.setQuantity(qty);
      return this;
    }

    public MediaRequestBuilder withTennPercentageQty(final Integer qty) {
      teenPercentage.setQuantity(qty);
      return this;
    }

    public MediaRequestBuilder withPreTeenPercentageQty(final Integer qty) {
      preTeenPercentage.setQuantity(qty);
      return this;
    }

    public MediaRequestBuilder withBoyPercentageQty(final Integer qty) {
      boyPercentage.setQuantity(qty);
      return this;
    }

        /*
            SEX Segmentation
         */

    public MediaRequestBuilder withFemalePercentageQty(final Integer qty) {
      femalePercentage.setQuantity(qty);
      return this;
    }

    public MediaRequestBuilder withMalePercentageQty(final Integer qty) {
      malePercentage.setQuantity(qty);
      return this;
    }

    /*
    public MediaRequestBuilder ensureCallSetters() {
      femalePercentage.setFemale(Sex.F);
      malePercentage.setMale(Sex.M);

      ageSegmentation.setAncientPercentage(ancientPercentage);
      ageSegmentation.setAdultPercentage(adultPercentage);
      ageSegmentation.setPreAdultPercentage(preAdultPercentage);
      ageSegmentation.setTeenPercentage(teenPercentage);
      ageSegmentation.setPreTeenPercentage(preTeenPercentage);
      ageSegmentation.setBoyPercentage(boyPercentage);

      sexSegmentation.setFemalePercentage(femalePercentage);
      sexSegmentation.setMalePercentage(malePercentage);

      nseSegmentationPercentage.setEmergencyNse(emergencyNse);
      nseSegmentationPercentage.setLowNse(lowNse);
      nseSegmentationPercentage.setMediumLowNse(mediumLowNse);
      nseSegmentationPercentage.setMediumNse(mediumNse);
      nseSegmentationPercentage.setMediumHighNse(mediumHighNse);
      nseSegmentationPercentage.setHighNse(highNse);

      segmentation.setAgeSegmentation(ageSegmentation);
      segmentation.setNseSegmentationPercentage(nseSegmentationPercentage);
      segmentation.setSexSegmentation(sexSegmentation);

      supporter.setSegmentation(segmentation);
      company.setSupporter(supporter);
      return this;
    }*/

    public MediaRequest build() {
      mediaRequest.setCompany(company);
      return mediaRequest;
    }
  }
}
