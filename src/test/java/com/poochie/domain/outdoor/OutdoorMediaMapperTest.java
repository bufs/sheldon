package com.poochie.domain.outdoor;

import com.poochie.domain.excel.OutdoorSheet;
import org.junit.Test;

import static com.poochie.domain.segmentation.SegmentationHelper.BOYS;
import static com.poochie.domain.segmentation.SegmentationHelper.HIGH;
import static com.poochie.domain.segmentation.SegmentationHelper.MALE;
import static com.poochie.domain.segmentation.SegmentationHelper.TEENAGER;
import static org.assertj.core.api.Assertions.assertThat;

public class OutdoorMediaMapperTest {

  private OutdoorMediaMapper outdoorMediaMapper = new OutdoorMediaMapper();

  @Test
  public void shouldReturnOutdoorObject() {
    OutdoorSheet sheet = createOutdoorSheet("rosario", "centro", "www/com",
            true);

    OutdoorMedia media = outdoorMediaMapper.transformToDomainObject(sheet);

    assertThat(media).isNotNull();
    assertThat(media.getSupporter().getIlluminated()).isTrue();
    assertThat(media.getArea()).isEqualTo("rosario");
  }

  private OutdoorSheet createOutdoorSheet(final String area, final String sector,
                                          final String urlToPublish, final boolean isIlluminated) {
    OutdoorSheet sheet = new OutdoorSheet();

    sheet.setMediaId("1");
    sheet.setUrlToPublish(urlToPublish);
    sheet.setSupporter("supporter");
    sheet.setSize("23x45");
    sheet.setImageUri("someimage.com/here.jpg");
    sheet.setVideoUri("someimage.com/video.jpg");
    sheet.setIlluminated(isIlluminated);
    sheet.setArea(area);
    sheet.setSector(sector);
    sheet.setSexSegmentation(String.format("%s", MALE));
    sheet.setAgeSegmentation(String.format("%s, %s", BOYS, TEENAGER));
    sheet.setNseSegmentation(HIGH);
    return sheet;
  }
}
