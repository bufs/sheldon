package com.poochie.domain.segmentation;

import com.poochie.domain.segmentation.age.AgeSegmentation;
import com.poochie.domain.segmentation.nse.NseSegmentation;
import com.poochie.domain.segmentation.sex.SexSegmentation;
import org.junit.Test;

import static com.poochie.domain.segmentation.SegmentationHelper.ADULT;
import static com.poochie.domain.segmentation.SegmentationHelper.ANCIENT;
import static com.poochie.domain.segmentation.SegmentationHelper.EMERGENCY;
import static com.poochie.domain.segmentation.SegmentationHelper.LOW;
import static com.poochie.domain.segmentation.SegmentationHelper.MEDIUM_LOW;
import static com.poochie.domain.segmentation.SegmentationHelper.PRE_ADULT;
import static com.poochie.domain.segmentation.SegmentationHelper.getAgeSegmentation;
import static com.poochie.domain.segmentation.SegmentationHelper.getNseSegmentation;
import static com.poochie.domain.segmentation.SegmentationHelper.getSexSegmentation;
import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;

public class SegmentationHelperTest {

  private static final String SEX_SEGMENTATION_SHEET_MEN = SegmentationHelper.MALE;
  private static final String SEX_SEGMENTATION_SHEET_BOTH = format("%s, %s", SegmentationHelper.MALE, SegmentationHelper.FEMALE);
  private static final String SEX_SEGMENTATION_SHEET_WOMEN = SegmentationHelper.FEMALE;

  @Test
  public void shouldReturnBothInSexSegmentation() {
    SexSegmentation sexSegmentation = getSexSegmentation(SEX_SEGMENTATION_SHEET_BOTH);

    assertThat(sexSegmentation.getToF()).isTrue();
    assertThat(sexSegmentation.getToM()).isTrue();
  }

  @Test
  public void shouldReturnMenInSexSegmentation() {
    SexSegmentation sexSegmentation = getSexSegmentation(SEX_SEGMENTATION_SHEET_MEN);

    assertThat(sexSegmentation.getToF()).isNull();
    assertThat(sexSegmentation.getToM()).isTrue();
  }

  @Test
  public void shouldReturnWomenInSexSegmentation() {
    SexSegmentation sexSegmentation = getSexSegmentation(SEX_SEGMENTATION_SHEET_WOMEN);

    assertThat(sexSegmentation.getToF()).isTrue();
    assertThat(sexSegmentation.getToM()).isNull();
  }

  @Test
  public void shouldReturnEmergency_Low_MediumLow_Nse() {
    NseSegmentation nseSegmentation = getNseSegmentation(
            format("%s, %s, %s", EMERGENCY, LOW, MEDIUM_LOW)
    );

    assertThat(nseSegmentation.getEmergency()).isTrue();
    assertThat(nseSegmentation.getMediumLow()).isTrue();
    assertThat(nseSegmentation.getLow()).isTrue();
    assertThat(nseSegmentation.getMediumHigh()).isNull();
  }

  @Test
  public void shouldReturn_preAdult_adult_ancientAges() {
    AgeSegmentation ageSegmentation = getAgeSegmentation(
            format("%s, %s, %s", PRE_ADULT, ADULT, ANCIENT)
    );

    assertThat(ageSegmentation.getPreAdult()).isTrue();
    assertThat(ageSegmentation.getAdult()).isTrue();
    assertThat(ageSegmentation.getAncient()).isTrue();

    assertThat(ageSegmentation.getBoy()).isNull();
    assertThat(ageSegmentation.getTeenager()).isNull();
  }
}
